# Deployer

## Introduction

Application intended to ease the deployment process of Java  Web Application Resource (WAR files) in Tomcat environments.

Implemented as a REST service, exposes functionality through http endpoints based on JSON format interchange.

## Database

This project use an embedded H2 database.

## Documentation

TODO: Needed

## Getting started

First, we need the application deployed in some environment, we will assume is under `http://localhost:8080`.
We can use the embedded documentation in `http://localhost:8080/swagger-ui.html` or some other http tool such as cUrl.

### Setup

- **[POST] */setup***

Setup for first use. Here is where the root user information and basic environment paths are configured.
This setup can be executed more than once, but starting the 2nd attempt, the email must be the same provided in the first setup, if that email can not be remember, through the stdout will be provided.
As well, the environment paths are only setup during the first execution, for further update, there is a endpoint dedicated to environments properly documented.

Request body fields:

    - email [type: string, mandatory]: Email to identify Deployer's root user.
    - fullName [type: string, mandatory]: Full name of Deployer's root user.
    - javaHome [type: string, mandatory]: Full path to JDK home.
    - mavenHome [type: string, mandatory]: Full path to Maven home.

Request body example:

```
{
  "email": "my@email.com",
  "fullName": "My Name",
  "javaHome": "/usr/lib/jvm/java-8-oracle",
  "mavenHome": "/usr/share/maven"
}
```

Request cUrl example command:

```
curl -X POST \
  http://localhost:8080/setup \
  -H 'Accept: */*' \
  -H 'Content-Type: application/json' \
  -d '{ "email": "my@email.com", "fullName": "My Name", "javaHome": "/usr/lib/jvm/java-8-oracle", "mavenHome": "/usr/share/maven"}'
```

Response example:

```
{
  "timestamp": "2019-04-20T19:18:01.469+0000",
  "message": "Setup ended successfully. Password was printed in console, it is advisable to change it as soon as possible"
}
```

### First login

- **[POST] */login***

After the initial setup, the first password was printed in the Deployer's stdout. It is highly advice to change this password as son as possible.

Request body fields:

    - email [type: string, mandatory]: Email provided to the setup.
    - password [type: string, mandatory]: Password printed in Deployer's stdout.

Request body example:

```
{
  "email": "my@email.com",
  "password": "cee0209b732c4fd2b596f3d8b463dfca"
}
```

Request cUrl example command:

```
curl -X POST \
  http://localhost:8080/login \
  -H 'Content-Type: application/json' \
  -H 'accept: */*' \
  -d '{ "email": "my@email.com", "password": "cee0209b732c4fd2b596f3d8b463dfca"}'
```

Response example:

```
{
  "id": 2,
  "created": "2019-04-20T19:18:01.365+0000",
  "updated": null,
  "deleted": null,
  "role": "SUPER",
  "fullName": "My Name",
  "email": "my@email.com",
  "password": null,
  "token": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJteUBlbWFpbC5jb20iLCJ1c2VySWQiOjIsImlhdCI6MTU1NTc4OTE4OX0.uhTx0XBQTBYviJ0F1KJ4Ww2_bCU0XI2JbnP2WctrXtja23IwTB2bRJbOVVz03IvC7Pd6lAXyLDLSkE8pDnd7zw"
}
```

From this response is that the authentication token can be obtained. This is set in the header in the following way: `Authorization:Bearer {token}`

### Change first password

- **[PUT] */users/{userId}/password***

After the initial login, it is highly advisable to change the password, here is how.

Request url fields:

    - userId [type: long, mandatory]: User id get in the login step.

Request header fields:

    - Authorization [type: string, mandatory]: Bearer token obtained in the login preced by Bearer and an space
    
Request body fields:

    - password [type: string, mandatory]: New password for user.

Request body example:

```
{
  "password": "123456"
}
```

Request cUrl example command:

```
curl -X PUT \
  http://localhost:8080/users/2/password \
  -H 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJteUBlbWFpbC5jb20iLCJ1c2VySWQiOjIsImlhdCI6MTU1NTc4OTE4OX0.uhTx0XBQTBYviJ0F1KJ4Ww2_bCU0XI2JbnP2WctrXtja23IwTB2bRJbOVVz03IvC7Pd6lAXyLDLSkE8pDnd7zw' \
  -H 'Content-Type: application/json' \
  -H 'accept: */*' \
  -d '{ "password": "123456"}'
```

Response example:

```
{
  "timestamp": "2019-04-20T19:48:20.339+0000",
  "message": "Password updated"
}
```

### Create SSH Key

- **[POST] */keys***

[Optional step] If the target repository required a level of security, is most than likely that is through a ssh key.

Request header fields:

    - Authorization [type: string, mandatory]: Bearer token obtained in the login preced by Bearer and an space

Request body fields:

    - alias [type: string, mandatory]: Alias to identify ssh key.

Request body example:

```
{
  "alias": "key-alias"
}
```

Request cUrl example command:

```
curl -X POST \
  http://localhost:8080/keys \
  -H 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJteUBlbWFpbC5jb20iLCJ1c2VySWQiOjIsImlhdCI6MTU1NTc4OTE4OX0.uhTx0XBQTBYviJ0F1KJ4Ww2_bCU0XI2JbnP2WctrXtja23IwTB2bRJbOVVz03IvC7Pd6lAXyLDLSkE8pDnd7zw' \
  -H 'Content-Type: application/json' \
  -H 'accept: */*' \
  -d '{ "alias": "key-alias"}'
```

Response example:

```
{
  "id": 3,
  "created": "2019-04-23T00:54:29.411+0000",
  "updated": null,
  "deleted": null,
  "publicKey": "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAAAgQCt0aJf+bL3yOZlsr1gHAZvyfFwU7l6w8g7QrLw65G870oQquv7bpsk5d/qPrFiz0YOsdxzGRHzjXGfdhu0KUZ5liTESb6cV9PMUov14bLt6s5WHrk4qPC9Z3x8+hUR8fQ4o+WdrIJWxCX4s83xrX55gEELLc1gStYTBm6gDrv5cw== key-alias",
  "alias": "key-alias"
}
```

From the publicKey field is where the public key can be obtained and deployed in the git repository.

### Create project

- **[POST] */projects***

From here the repository is cloned and managed

Request header fields:

    - Authorization [type: string, mandatory]: Bearer token obtained in the login preced by Bearer and an space

Request query fields:

    - sshKeyId [type: long, optional]: Ssh key id to be use to clone and pull git repository.

Request body fields:

    - alias [type: string, mandatory]: Alias to identify the project.
    - name [type: string, mandatory]: Project's name.
    - url [type: string, mandatory]: Ssh url for the project's repository.
    - branch [type: string, mandatory]: Branch to clone from repository and observed to trigger build process.
    - goals [type: string, mandatory]: Maven goals to be executed, each must be coma separated from the other.
    - packagedPath [type: string, mandatory]: Path, from the project root, to the packaged file.
    - targetPath [type: string, mandatory]: Full path to copy the packaged file into.

Request body example:

```
{
  "alias": "deployer",
  "branch": "master",
  "goals": "clean,packaged",
  "name": "Tomcat WAT Deployer",
  "packagedPath": "target/deployer-0.0.1-SNAPSHOT.war",
  "targetPath": "/tmp/deployer.war",
  "url": "git@github.com:fsQuiroz/deployer.git"
}
```

Request cUrl example command:

```
curl -X POST \
  'http://localhost:8080/projects?sshKeyId=3' \
  -H 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJteUBlbWFpbC5jb20iLCJ1c2VySWQiOjIsImlhdCI6MTU1NTc4OTE4OX0.uhTx0XBQTBYviJ0F1KJ4Ww2_bCU0XI2JbnP2WctrXtja23IwTB2bRJbOVVz03IvC7Pd6lAXyLDLSkE8pDnd7zw' \
  -H 'Content-Type: application/json' \
  -H 'accept: */*' \
  -d '{ "alias": "deployer", "branch": "master", "goals": "clean,packaged", "name": "Tomcat WAT Deployer", "packagedPath": "target/deployer-0.0.1-SNAPSHOT.war", "targetPath": "/tmp/deployer.war", "url": "git@github.com:fsQuiroz/deployer.git"}'
```

Response example:

```
{
  "id": 5,
  "created": "2019-04-23T01:09:25.339+0000",
  "updated": null,
  "deleted": null,
  "name": "Tomcat WAT Deployer",
  "alias": "deployer",
  "url": "git@github.com:fsQuiroz/deployer.git",
  "branch": "master",
  "goals": "clean,packaged",
  "packagedPath": "target/deployer-0.0.1-SNAPSHOT.war",
  "targetPath": "/tmp/deployer.war",
  "key": {
    "id": 3,
    "created": "2019-04-23T00:54:29.411+0000",
    "updated": null,
    "deleted": null,
    "publicKey": null,
    "alias": "key-alias"
  }
}
```

On response 201 CREATED, the project has been successfully clone.

### Create CLIENT user

- **[POST] */users***

It is advisable to create and specific user to trigger the update on a particular project, or all of them.

Request header fields:

    - Authorization [type: string, mandatory]: Bearer token obtained in the login preced by Bearer and an space

Request body fields:

    - email [type: string, mandatory]: Email to identify client's user.
    - fullName [type: string, mandatory]: Full name of client's user.
    - password [type: string, optional]: New password for user. Mandatory if role is not CLIENT.
    - role [type: string, mandatory]: Role to the new user [CLIENT|ADMIN].

Request body example:

```
{
  "email": "client@email.com",
  "fullName": "Webhook client",
  "role": "CLIENT"
}
```

Request cUrl example command:

```
curl -X POST \
  http://localhost:8080/users \
  -H 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJteUBlbWFpbC5jb20iLCJ1c2VySWQiOjIsImlhdCI6MTU1NTc4OTE4OX0.uhTx0XBQTBYviJ0F1KJ4Ww2_bCU0XI2JbnP2WctrXtja23IwTB2bRJbOVVz03IvC7Pd6lAXyLDLSkE8pDnd7zw' \
  -H 'Content-Type: application/json' \
  -H 'accept: */*' \
  -d '{ "email": "client@email.com", "fullName": "Webhook client", "role": "CLIENT"}'
```

Response example:

```
{
  "id": 6,
  "created": "2019-04-23T01:18:13.943+0000",
  "updated": null,
  "deleted": null,
  "role": "CLIENT",
  "fullName": "Webhook client",
  "email": "client@email.com",
  "password": null,
  "token": null
}
```

In the case of CLIENT users, they do not have any privilege at all, at least a authorization token is manually created.

### Create token for CLIENT

- **[GET] */users/{userId}/token***

In order to authorize the CLIENT user to trigger an update on a project, a authorization token must be created.

Request url fields:

    - userId [type: long, mandatory]: User id generate token. Must be user with CLIENT role.

Request query fields:

    - projectId [type: long, optional]: If present, the update trigger can only be perform on the especify project.

Request header fields:

    - Authorization [type: string, mandatory]: Bearer token obtained in the login preced by Bearer and an space

Request cUrl example command:

```
curl -X GET \
  'http://localhost:8080/users/6/token?projectId=5' \
  -H 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJteUBlbWFpbC5jb20iLCJ1c2VySWQiOjIsImlhdCI6MTU1NTc4OTE4OX0.uhTx0XBQTBYviJ0F1KJ4Ww2_bCU0XI2JbnP2WctrXtja23IwTB2bRJbOVVz03IvC7Pd6lAXyLDLSkE8pDnd7zw' \
  -H 'accept: */*'
```

Response example:

```
{
  "id": 6,
  "created": "2019-04-23T01:18:13.943+0000",
  "updated": null,
  "deleted": null,
  "role": "CLIENT",
  "fullName": "Webhook client",
  "email": "client@email.com",
  "password": null,
  "token": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJjbGllbnRAZW1haWwuY29tIiwidXNlcklkIjo2LCJpYXQiOjE1NTU5ODcxODUsInByb2plY3RJZCI6NX0.UqPWePwuGzNaKLBuZrxjEtg0KUzFVIaXPMq_HzGO7z4SNXhuaRMmUbsZuXM7MUU9sYDaIJ6yFkg8D4xtjl8M_w"
}
```

The token generated has permission only to trigger an update on any project, or specific project only if set in projectId query param.
In the case of GitLab.com, a Webhook can be easily setup under integration, setting the URL to the public DNS of the deployer instantiation (domain.example/deployer/projects/{projectId}/build), paste the token param in the response with Bearer and space, configure the Trigger for Push, under the same branch in the creation of the project.


### Trigger update

- **[POST] */projects/{projectId}/builds***

This action trigger the pull of the project repository's branch and maven goals executions, lately copy from packagedPath to targetPath.

Request url fields:

    - projectId [type: long, mandatory]: Project to trigger update.

Request header fields:

    - Authorization [type: string, mandatory]: Bearer token obtained in the login preced by Bearer and an space

Request cUrl example command:

```
curl -X POST \
  http://localhost:8080/projects/5/builds \
  -H 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJteUBlbWFpbC5jb20iLCJ1c2VySWQiOjIsImlhdCI6MTU1NTc4OTE4OX0.uhTx0XBQTBYviJ0F1KJ4Ww2_bCU0XI2JbnP2WctrXtja23IwTB2bRJbOVVz03IvC7Pd6lAXyLDLSkE8pDnd7zw' \
  -H 'accept: */*'
```

Response example:

```
{
  "timestamp": "2019-04-23T02:57:40.509+0000",
  "message": "Build triggered"
}
```

### The rest

All endpoints are fully documented in Swagger, exposing all the flexibility of this tool.
