package com.fsquiroz.deployer;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;

@SpringBootTest()
@TestPropertySource(locations = "classpath:application-test.properties")
@ActiveProfiles("test")
public class TomcatWarDeployerApplicationTests {

    @Test
    public void contextLoads() {
    }

}
