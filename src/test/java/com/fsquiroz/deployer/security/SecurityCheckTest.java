package com.fsquiroz.deployer.security;

import com.fsquiroz.deployer.entity.db.User;
import com.fsquiroz.deployer.exception.NotFoundException;
import com.fsquiroz.deployer.exception.UnauthorizedException;
import com.fsquiroz.deployer.repository.UserRepository;
import io.jsonwebtoken.JwtException;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Calendar;
import java.util.Date;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.when;

@Slf4j
@ExtendWith(MockitoExtension.class)
public class SecurityCheckTest {

    private static final String JWT_SECRET = "nrJHGZ36C4XSDZlrDNl3L5jP9AJI3vm7ToG+YvxETaJyttJgn7liisCC7ItZ1ziYDhisd856DwSnmstG9Ych7Q==";
    private static final String JWT_ALG = "HmacSHA512";

    @Mock
    private UserRepository repository;

    private SecurityCheck securityCheck;

    @BeforeEach
    public void setUp() {
        securityCheck = new SecurityCheck(repository, JWT_SECRET, JWT_ALG);
    }

    @AfterEach
    public void tearDown() {
        SecurityContextHolder.clearContext();
    }

    @Test
    public void getUser() {
        log.info("Test get User");

        User u = new User();
        u.setId(1L);
        u.setFullName("Some Name");

        when(repository.findById(u.getId())).thenReturn(Optional.of(u));

        User response = securityCheck.get(1L);

        assertThat(response)
                .isEqualTo(u);
    }

    @Test
    public void getUserNotFound() {
        log.info("Test get User not found");

        assertThatThrownBy(() -> securityCheck.get(1L))
                .isInstanceOf(NotFoundException.class);
    }

    @Test
    public void getUserDeleted() {
        log.info("Test get User deleted");

        User u = new User();
        u.setId(1L);
        u.setDeleted(new Date());
        u.setFullName("Some Name");

        when(repository.findById(u.getId())).thenReturn(Optional.of(u));

        assertThatThrownBy(() -> securityCheck.get(1L))
                .isInstanceOf(UnauthorizedException.class);
    }

    @Test
    public void authorized() {
        log.info("Test authorized with token");

        User u = new User();
        u.setId(1L);
        u.setEmail("some@email.com");
        u.setFullName("Some Name");

        when(repository.findById(u.getId())).thenReturn(Optional.of(u));
        String token = securityCheck.generateToken(null, null, null, u, null);
        User response = securityCheck.authenticate(token);

        assertThat(response)
                .isEqualTo(u);
    }

    @Test
    public void authorizedNotExpired() {
        log.info("Test authorized with token not expired");

        User u = new User();
        u.setId(1L);
        u.setEmail("some@email.com");
        u.setFullName("Some Name");
        Date future = date(1);

        when(repository.findById(u.getId())).thenReturn(Optional.of(u));

        String token = securityCheck.generateToken(null, future, null, u, null);
        User response = securityCheck.authenticate(token);

        assertThat(response)
                .isEqualTo(u);
    }

    @Test
    public void authorizedExpired() {
        log.info("Test authorized with token expired");

        User u = new User();
        u.setId(1L);
        u.setEmail("some@email.com");
        u.setFullName("Some Name");
        Date past = date(-1);

        String token = securityCheck.generateToken(null, past, null, u, null);

        assertThatThrownBy(() -> securityCheck.authenticate(token))
                .isInstanceOf(JwtException.class);
    }

    @Test
    public void authorizedNotReady() {
        log.info("Test authorized with token not ready");

        User u = new User();
        u.setId(1L);
        u.setEmail("some@email.com");
        u.setFullName("Some Name");
        Date future = date(1);

        String token = securityCheck.generateToken(null, null, future, u, null);

        assertThatThrownBy(() -> securityCheck.authenticate(token))
                .isInstanceOf(JwtException.class);
    }

    @Test
    public void authorizedReady() {
        log.info("Test authorized with token ready");

        User u = new User();
        u.setId(1L);
        u.setEmail("some@email.com");
        u.setFullName("Some Name");
        Date past = date(-1);

        when(repository.findById(u.getId())).thenReturn(Optional.of(u));

        String token = securityCheck.generateToken(null, null, past, u, null);
        var response = securityCheck.authenticate(token);

        assertThat(response).isNotNull();
    }

    @Test
    public void getAuthentication() {
        log.info("Test get authenticated user");

        User u = new User();
        u.setId(1L);
        u.setEmail("some@email.com");
        u.setFullName("Some Name");
        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(u, null, u.getAuthorities()));

        User response = securityCheck.getAuthentication();

        assertThat(response)
                .isEqualTo(u);
    }

    @Test
    public void getNotAuthenticationUser() {
        log.info("Test get anonymous user");

        assertThatThrownBy(() -> securityCheck.getAuthentication())
                .isExactlyInstanceOf(UnauthorizedException.class);
    }

    private Date date(int dayVariation) {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DAY_OF_YEAR, dayVariation);
        return c.getTime();
    }

}
