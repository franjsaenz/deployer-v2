package com.fsquiroz.deployer.service;

import com.fsquiroz.deployer.entity.db.SshKeyPair;
import com.fsquiroz.deployer.entity.json.MSshKeyPair;
import com.fsquiroz.deployer.repository.ProjectRepository;
import com.fsquiroz.deployer.repository.SshKeyPairRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class SshKeyPairMockService extends SshKeyPairService {

    public SshKeyPairMockService(
            SshKeyPairRepository sshKeyPairRepository,
            ProjectRepository projectRepository,
            @Value("${com.fsquiroz.deployer.sshkeys.path}") final String path
    ) {
        super(sshKeyPairRepository, projectRepository, path);
    }

    @Override
    public MSshKeyPair build(SshKeyPair domain, boolean withPublicKey) {
        return super.build(domain, false);
    }

}
