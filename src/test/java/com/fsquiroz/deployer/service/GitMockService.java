package com.fsquiroz.deployer.service;

import com.fsquiroz.deployer.entity.db.Project;
import com.fsquiroz.deployer.service.declaration.IGitService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class GitMockService implements IGitService {

    @Override
    public void clone(Project p) {
        log.info("MOCK: Cloning project id '{}' name '{}'", p.getId(), p.getName());
        log.info("MOCK: Clone process finish");
    }

    @Override
    public void update(Project p) {
        log.info("MOCK: Updating project id '{}' name '{}'", p.getId(), p.getName());
        log.info("MOCK: Update process finish");
    }

    @Override
    public void checkout(Project p) {
        log.info("MOCK: Checking out project id '{}' name '{}' to branch '{}'", p.getId(), p.getName(), p.getBranch());
        log.info("MOCK: Checkout process finish");
    }

    @Override
    public void reset(Project p) {
        log.info("MOCK: Resetting project id '{}' name '{}'", p.getId(), p.getName());
        log.info("MOCK: Reset process finish");
    }

    @Override
    public void pull(Project p) {
        log.info("MOCK: Pulling project id '{}' name '{}' branch '{}'", p.getId(), p.getName(), p.getBranch());
        log.info("MOCK: Pull process finish");
    }

}
