package com.fsquiroz.deployer.service;

import com.fsquiroz.deployer.entity.db.User;
import com.fsquiroz.deployer.entity.json.MSetup;
import com.fsquiroz.deployer.repository.UserRepository;
import com.fsquiroz.deployer.security.SecurityCheck;
import com.fsquiroz.deployer.service.declaration.ISettingsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserMockService extends UserService {

    private PasswordEncoder passwordEncoder;

    private UserRepository userRepository;

    public UserMockService(
            ISettingsService settingsService,
            SecurityCheck securityCheck,
            PasswordEncoder passwordEncoder,
            UserRepository userRepository
    ) {
        super(settingsService, securityCheck, passwordEncoder, userRepository);
        this.passwordEncoder = passwordEncoder;
        this.userRepository = userRepository;
    }

    @Override
    public User setup(MSetup ms) {
        User u = super.setup(ms);
        u.setPassword(passwordEncoder.encode("setup-password"));
        return userRepository.save(u);
    }
}
