package com.fsquiroz.deployer.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fsquiroz.deployer.entity.db.Project;
import com.fsquiroz.deployer.entity.db.User;
import com.fsquiroz.deployer.repository.ProjectRepository;
import com.fsquiroz.deployer.service.declaration.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class ProjectMockService extends ProjectService {

    public ProjectMockService(
            ISshKeyPairService sshKeyPairService,
            IBuildService buildService,
            IGitService gitService,
            IMavenService mavenService,
            ISchedulerService schedulerService,
            ProjectRepository projectRepository,
            @Value("${com.fsquiroz.deployer.projects.path}") final String projectPath
    ) {
        super(sshKeyPairService, buildService, gitService, mavenService, schedulerService, projectRepository, projectPath);
    }

    @Override
    public void triggerUpdate(User user, Project p, JsonNode json) {
        super.triggerUpdate(user, p, json, true);
    }

}
