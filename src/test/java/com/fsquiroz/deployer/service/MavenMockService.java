package com.fsquiroz.deployer.service;

import com.fsquiroz.deployer.entity.db.Project;
import com.fsquiroz.deployer.service.declaration.IMavenService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class MavenMockService implements IMavenService {

    @Override
    public void validatePom(Project p) {
        log.info("MOCK: Validating pom");
    }

    @Override
    public void execute(Project p, List<String> goals) {
        log.info("MOCK: Executing maven on project id '{}' name '{}' with goals '{}'", p.getId(), p.getName(), goals);
        log.info("MOCK: Maven process finish");
    }

}
