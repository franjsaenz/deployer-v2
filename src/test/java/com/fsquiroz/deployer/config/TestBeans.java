package com.fsquiroz.deployer.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.password.PasswordEncoder;

import static java.util.Optional.ofNullable;

@Configuration
@Profile("test")
@Slf4j
public class TestBeans {

    @Bean
    @Primary
    public PasswordEncoder passwordEncoder() {
        return new PasswordEncoder() {

            @Override
            public String encode(CharSequence rawPassword) {
                return ofNullable(rawPassword).map(String::valueOf).orElse(null);
            }

            @Override
            public boolean matches(CharSequence rawPassword, String encodedPassword) {
                return rawPassword == null ? encodedPassword == null : rawPassword.toString().equals(encodedPassword);
            }
        };
    }

}
