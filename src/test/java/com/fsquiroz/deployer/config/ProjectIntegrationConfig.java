package com.fsquiroz.deployer.config;

import com.fsquiroz.deployer.entity.db.Build;
import com.fsquiroz.deployer.entity.db.BuildStatus;
import com.fsquiroz.deployer.entity.db.Project;
import com.fsquiroz.deployer.repository.BuildRepository;
import com.fsquiroz.deployer.repository.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ActiveProfiles;

import java.util.Date;

@Component
@ActiveProfiles("test")
public class ProjectIntegrationConfig {

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private BuildRepository buildRepository;

    private Project projectOne;

    private Project projectTwo;

    public void setup() {
        projectOne = new Project();
        projectOne.setCreated(new Date());
        projectOne.setAlias("test-project");
        projectOne.setName("Test project");
        projectOne.setUrl("some-url");
        projectOne.setBranch("some-branch");
        projectOne.setGoals("some,goals");
        projectOne.setTargetPath("/some/path");
        projectOne.setPackagedPath("some/path");
        projectOne.setKey(null);
        projectOne = projectRepository.save(projectOne);

        projectTwo = new Project();
        projectTwo.setCreated(new Date());
        projectTwo.setAlias("test-project-two");
        projectTwo.setName("Test project Two");
        projectTwo.setUrl("some-url");
        projectTwo.setBranch("some-branch");
        projectTwo.setGoals("some,goals");
        projectTwo.setTargetPath("/some/path");
        projectTwo.setPackagedPath("some/path");
        projectTwo.setKey(null);
        projectTwo = projectRepository.save(projectTwo);
    }

    public void cleanup() {
        this.buildRepository.deleteAll();
        this.projectRepository.deleteAll();
    }

    public Project getProjectOne() {
        return projectOne;
    }

    public Project getProjectTwo() {
        return projectTwo;
    }

    public Build build(Project p) {
        Build b = new Build();
        b.setCreated(new Date(System.currentTimeMillis() - (10 * 1000)));
        b.setProject(p);
        b.setStart(b.getCreated());
        b.setUpdated(new Date());
        b.setMessage(null);
        b.setEnd(new Date());
        b.setStatus(BuildStatus.OK);
        return buildRepository.save(b);
    }

}
