package com.fsquiroz.deployer.config;

import com.fsquiroz.deployer.entity.db.Project;
import com.fsquiroz.deployer.entity.db.Role;
import com.fsquiroz.deployer.entity.db.User;
import com.fsquiroz.deployer.repository.UserRepository;
import com.fsquiroz.deployer.security.SecurityCheck;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import java.util.Date;

@Component
@ActiveProfiles("test")
public class UserIntegrationConfig {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private SecurityCheck securityCheck;

    private User root;

    private User admin;

    private User client;

    public void setup() {
        root = new User();
        root.setPassword(passwordEncoder.encode("123456"));
        root.setEmail("super@test.com");
        root.setFullName("Super Test");
        root.setRole(Role.SUPER);
        root.setCreated(new Date());
        root = userRepository.save(root);
        admin = new User();
        admin.setPassword(passwordEncoder.encode("123456"));
        admin.setEmail("admin@test.com");
        admin.setFullName("Admin Test");
        admin.setRole(Role.ADMIN);
        admin.setCreated(new Date());
        admin = userRepository.save(admin);
        client = new User();
        client.setEmail("client@test.com");
        client.setFullName("Client Test");
        client.setRole(Role.CLIENT);
        client.setCreated(new Date());
        client = userRepository.save(client);
    }

    public void cleanup() {
        userRepository.deleteAll();
    }

    public void authenticate(MockHttpServletRequestBuilder request, User user) {
        this.authenticate(request, user, null);
    }

    public void authenticate(MockHttpServletRequestBuilder request, User user, Project project) {
        if (user != null) {
            String token = securityCheck.generateToken(null, null, null, user, project);
            request.header("Authorization", "Bearer " + token);
        }
    }

    public User getRoot() {
        return root;
    }

    public User getAdmin() {
        return admin;
    }

    public User getClient() {
        return client;
    }

    public void delete(User u) {
        u.setDeleted(new Date());
        userRepository.save(u);
    }

}
