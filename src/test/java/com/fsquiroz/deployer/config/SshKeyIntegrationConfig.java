package com.fsquiroz.deployer.config;

import com.fsquiroz.deployer.entity.db.SshKeyPair;
import com.fsquiroz.deployer.repository.SshKeyPairRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ActiveProfiles;

import java.util.Date;

@Component
@ActiveProfiles("test")
public class SshKeyIntegrationConfig {

    @Autowired
    private SshKeyPairRepository sshKeyPairRepository;

    private SshKeyPair key;

    public void setup() {
        key = new SshKeyPair();
        key.setAlias("test-key");
        key.setCreated(new Date());
        key.setPrivateKey("/no/path");
        key.setPublicKey("/no/path");
        key = sshKeyPairRepository.save(key);
    }

    public void cleanup() {
        sshKeyPairRepository.deleteAll();
    }

    public SshKeyPair getKey() {
        return key;
    }

}
