package com.fsquiroz.deployer.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fsquiroz.deployer.config.UserIntegrationConfig;
import com.fsquiroz.deployer.entity.db.User;
import com.fsquiroz.deployer.entity.json.MLogin;
import com.fsquiroz.deployer.entity.json.MSetup;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-test.properties")
@ActiveProfiles("test")
@Slf4j
public class PublicControllerIntegration {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private UserIntegrationConfig userIntegrationConfig;

    @AfterEach
    public void cleanup() {
        this.userIntegrationConfig.cleanup();
    }

    @Test
    public void loginAsRoot() throws Exception {
        log.info("Test login as root");
        userIntegrationConfig.setup();
        User who = userIntegrationConfig.getRoot();
        MLogin login = new MLogin(who.getEmail(), "123456");
        mockMvc.perform(login(login))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void loginAsAdmin() throws Exception {
        log.info("Test login as admin");
        userIntegrationConfig.setup();
        User who = userIntegrationConfig.getAdmin();
        MLogin login = new MLogin(who.getEmail(), "123456");
        mockMvc.perform(login(login))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void loginAsClient() throws Exception {
        log.info("Test login as client");
        userIntegrationConfig.setup();
        User who = userIntegrationConfig.getClient();
        MLogin login = new MLogin(who.getEmail(), "123456");
        mockMvc.perform(login(login))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void loginAsNoUser() throws Exception {
        log.info("Test login as no user");
        MLogin login = new MLogin("not@found.com", "123456");
        mockMvc.perform(login(login))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void loginAsDeleted() throws Exception {
        log.info("Test login as user deleted");
        userIntegrationConfig.setup();
        User who = userIntegrationConfig.getAdmin();
        userIntegrationConfig.delete(who);
        MLogin login = new MLogin(who.getEmail(), "123456");
        mockMvc.perform(login(login))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void loginWithoutEmail() throws Exception {
        log.info("Test login without emails");
        MLogin login = new MLogin(null, "123456");
        mockMvc.perform(login(login))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void loginWithoutPassword() throws Exception {
        log.info("Test login without passwod");
        MLogin login = new MLogin("not@found.com", null);
        mockMvc.perform(login(login))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void loginWithWrongPassword() throws Exception {
        log.info("Test login with wrong password");
        userIntegrationConfig.setup();
        User who = userIntegrationConfig.getAdmin();
        MLogin login = new MLogin(who.getEmail(), "wrong");
        mockMvc.perform(login(login))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void setupSetupFirstTime() throws Exception {
        log.info("Test setup for the first time");
        MSetup ms = new MSetup("Full Name", "full@name.com", "/java/home", "/maven/home");
        mockMvc.perform(setup(ms))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void setupSetupSecondTime() throws Exception {
        log.info("Test setup for the second time");
        userIntegrationConfig.setup();
        User root = userIntegrationConfig.getRoot();
        MSetup ms = new MSetup(root.getFullName(), root.getEmail(), "/java/home", "/maven/home");
        mockMvc.perform(setup(ms))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void setupSetupSecondTimeWrongEmail() throws Exception {
        log.info("Test setup for the second time with wrong email");
        userIntegrationConfig.setup();
        User root = userIntegrationConfig.getRoot();
        MSetup ms = new MSetup(root.getFullName(), "wrong@email.com", "/java/home", "/maven/home");
        mockMvc.perform(setup(ms))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    private MockHttpServletRequestBuilder login(MLogin login) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .post("/login")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(login));
        return request;
    }

    private MockHttpServletRequestBuilder setup(MSetup setup) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .post("/setup")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(setup));
        return request;
    }

}
