package com.fsquiroz.deployer.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fsquiroz.deployer.config.UserIntegrationConfig;
import com.fsquiroz.deployer.entity.db.Settings;
import com.fsquiroz.deployer.entity.db.User;
import com.fsquiroz.deployer.entity.json.MSettings;
import com.fsquiroz.deployer.repository.SettingsRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Date;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-test.properties")
@ActiveProfiles("test")
@Slf4j
public class SettingsControllerIntegration {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private UserIntegrationConfig userIntegrationConfig;

    @Autowired
    private SettingsRepository settingsRepository;

    private User root;

    private User admin;

    private User client;

    @BeforeEach
    public void setup() {
        userIntegrationConfig.setup();
        this.root = userIntegrationConfig.getRoot();
        this.admin = userIntegrationConfig.getAdmin();
        this.client = userIntegrationConfig.getClient();
        Settings s = new Settings();
        s.setMavenHome("/path/to/maven");
        s.setJavaHome("/path/to/java");
        s.setCreated(new Date());
        s.setTimestamp(s.getCreated().getTime());
        settingsRepository.save(s);
    }

    @AfterEach
    public void cleanup() {
        this.userIntegrationConfig.cleanup();
        this.settingsRepository.deleteAll();
    }

    @Test
    public void getAsRoot() throws Exception {
        log.info("Test get settings as root");
        mockMvc.perform(get(root))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getAsAdmin() throws Exception {
        log.info("Test get settings as admin");
        mockMvc.perform(get(admin))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void getAsClient() throws Exception {
        log.info("Test get settings as client");
        mockMvc.perform(get(client))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void getAsAnonymous() throws Exception {
        log.info("Test get settings as anonymous");
        mockMvc.perform(get(null))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void updateAsRoot() throws Exception {
        log.info("Test update settings as root");
        MSettings ms = new MSettings();
        ms.setJavaHome("/new/to/java");
        ms.setMavenHome("/new/to/maven");
        mockMvc.perform(update(root, ms))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void updateAsAdmin() throws Exception {
        log.info("Test update settings as admin");
        MSettings ms = new MSettings();
        ms.setJavaHome("/new/to/java");
        ms.setMavenHome("/new/to/maven");
        mockMvc.perform(update(admin, ms))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void updateAsClient() throws Exception {
        log.info("Test update settings as client");
        MSettings ms = new MSettings();
        ms.setJavaHome("/new/to/java");
        ms.setMavenHome("/new/to/maven");
        mockMvc.perform(update(client, ms))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void updateAsAnonymous() throws Exception {
        log.info("Test update settings as anonymous");
        MSettings ms = new MSettings();
        ms.setJavaHome("/new/to/java");
        ms.setMavenHome("/new/to/maven");
        mockMvc.perform(update(null, ms))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    private MockHttpServletRequestBuilder get(User who) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .get("/settings")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder update(User who, MSettings ms) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .post("/settings")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(ms));
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

}
