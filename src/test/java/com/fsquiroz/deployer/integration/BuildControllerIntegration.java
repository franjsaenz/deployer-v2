package com.fsquiroz.deployer.integration;

import com.fsquiroz.deployer.config.ProjectIntegrationConfig;
import com.fsquiroz.deployer.config.UserIntegrationConfig;
import com.fsquiroz.deployer.entity.db.Build;
import com.fsquiroz.deployer.entity.db.Project;
import com.fsquiroz.deployer.entity.db.User;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-test.properties")
@ActiveProfiles("test")
@Slf4j
public class BuildControllerIntegration {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UserIntegrationConfig userIntegrationConfig;

    @Autowired
    private ProjectIntegrationConfig projectIntegrationConfig;

    private User root;

    private User admin;

    private User client;

    private Build build;

    @BeforeEach
    public void setup() {
        userIntegrationConfig.setup();
        projectIntegrationConfig.setup();
        this.root = userIntegrationConfig.getRoot();
        this.admin = userIntegrationConfig.getAdmin();
        this.client = userIntegrationConfig.getClient();
        Project projectOne = projectIntegrationConfig.getProjectOne();
        this.build = projectIntegrationConfig.build(projectOne);
    }

    @AfterEach
    public void cleanup() {
        this.userIntegrationConfig.cleanup();
        this.projectIntegrationConfig.cleanup();
    }

    @Test
    public void searchAsRoot() throws Exception {
        log.info("Test search builds as root");
        mockMvc.perform(search(root))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void searchAsAdmin() throws Exception {
        log.info("Test search builds as admin");
        mockMvc.perform(search(admin))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void searchAsClient() throws Exception {
        log.info("Test search builds as client");
        mockMvc.perform(search(client))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void searchAsAnonymous() throws Exception {
        log.info("Test search builds as anonymous");
        mockMvc.perform(search(null))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void getAsRoot() throws Exception {
        log.info("Test get build as root");
        mockMvc.perform(get(root, build))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getAsAdmin() throws Exception {
        log.info("Test get build as admin");
        mockMvc.perform(get(admin, build))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getAsClient() throws Exception {
        log.info("Test get build as client");
        mockMvc.perform(get(client, build))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void getAsAnonymous() throws Exception {
        log.info("Test get build as anonymous");
        mockMvc.perform(get(null, build))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    private MockHttpServletRequestBuilder search(User who) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .get("/builds")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder get(User who, Build toGet) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .get("/builds/{buildId}", toGet.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

}
