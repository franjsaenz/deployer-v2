package com.fsquiroz.deployer.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fsquiroz.deployer.config.ProjectIntegrationConfig;
import com.fsquiroz.deployer.config.UserIntegrationConfig;
import com.fsquiroz.deployer.entity.db.Project;
import com.fsquiroz.deployer.entity.db.User;
import com.fsquiroz.deployer.entity.json.MProject;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-test.properties")
@ActiveProfiles("test")
@Slf4j
public class ProjectControllerIntegration {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private UserIntegrationConfig userIntegrationConfig;

    @Autowired
    private ProjectIntegrationConfig projectIntegrationConfig;

    private User root;

    private User admin;

    private User client;

    private Project projectOne;

    private Project projectTwo;

    @BeforeEach
    public void setup() {
        userIntegrationConfig.setup();
        projectIntegrationConfig.setup();
        this.root = userIntegrationConfig.getRoot();
        this.admin = userIntegrationConfig.getAdmin();
        this.client = userIntegrationConfig.getClient();
        this.projectOne = projectIntegrationConfig.getProjectOne();
        this.projectTwo = projectIntegrationConfig.getProjectTwo();
        projectIntegrationConfig.build(projectTwo);
    }

    @AfterEach
    public void cleanup() {
        this.userIntegrationConfig.cleanup();
        this.projectIntegrationConfig.cleanup();
    }

    @Test
    public void searchAsRoot() throws Exception {
        log.info("Test search projects as root");
        mockMvc.perform(search(root))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void searchAsAdmin() throws Exception {
        log.info("Test search projects as admin");
        mockMvc.perform(search(admin))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void searchAsClient() throws Exception {
        log.info("Test search projects as client");
        mockMvc.perform(search(client))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void searchAsAnonymous() throws Exception {
        log.info("Test search projects as anonymous");
        mockMvc.perform(search(null))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void createAsRoot() throws Exception {
        log.info("Test create project as root");
        MProject mp = new MProject();
        mp.setName("Some other project");
        mp.setAlias("some-other-project");
        mp.setBranch("some-branch");
        mp.setGoals("some,goals");
        mp.setPackagedPath("some/packaged");
        mp.setTargetPath("/some/target");
        mp.setUrl("some-url");
        mockMvc.perform(create(root, mp))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void createAsAdmin() throws Exception {
        log.info("Test create project as admin");
        MProject mp = new MProject();
        mp.setName("Some other project");
        mp.setAlias("some-other-project");
        mp.setBranch("some-branch");
        mp.setGoals("some,goals");
        mp.setPackagedPath("some/packaged");
        mp.setTargetPath("/some/target");
        mp.setUrl("some-url");
        mockMvc.perform(create(admin, mp))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void createAsClient() throws Exception {
        log.info("Test create project as client");
        MProject mp = new MProject();
        mp.setName("Some other project");
        mp.setAlias("some-other-project");
        mp.setBranch("some-branch");
        mp.setGoals("some,goals");
        mp.setPackagedPath("some/packaged");
        mp.setTargetPath("/some/target");
        mp.setUrl("some-url");
        mockMvc.perform(create(client, mp))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void createAsAnonymous() throws Exception {
        log.info("Test create project as anonymous");
        MProject mp = new MProject();
        mp.setName("Some other project");
        mp.setAlias("some-other-project");
        mp.setBranch("some-branch");
        mp.setGoals("some,goals");
        mp.setPackagedPath("some/packaged");
        mp.setTargetPath("/some/target");
        mp.setUrl("some-url");
        mockMvc.perform(create(null, mp))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void getAsRoot() throws Exception {
        log.info("Test get project as root");
        mockMvc.perform(get(root, projectOne))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getAsAdmin() throws Exception {
        log.info("Test get project as admin");
        mockMvc.perform(get(admin, projectOne))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getAsClient() throws Exception {
        log.info("Test get project as client");
        mockMvc.perform(get(client, projectOne))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void getAsAnonymous() throws Exception {
        log.info("Test get project as anonymous");
        mockMvc.perform(get(null, projectOne))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void editAsRoot() throws Exception {
        log.info("Test edit project as root");
        MProject mp = new MProject();
        mp.setName("Some edit project");
        mp.setAlias("some-edit-project");
        mp.setBranch("some-branch");
        mp.setGoals("some,goals");
        mp.setPackagedPath("some/packaged");
        mp.setTargetPath("/some/target");
        mp.setUrl("some-url");
        mockMvc.perform(put(root, projectOne, mp))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void editAsAdmin() throws Exception {
        log.info("Test edit project as admin");
        MProject mp = new MProject();
        mp.setName("Some edit project");
        mp.setAlias("some-edit-project");
        mp.setBranch("some-branch");
        mp.setGoals("some,goals");
        mp.setPackagedPath("some/packaged");
        mp.setTargetPath("/some/target");
        mp.setUrl("some-url");
        mockMvc.perform(put(admin, projectOne, mp))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void editAsClient() throws Exception {
        log.info("Test edit project as client");
        MProject mp = new MProject();
        mp.setName("Some edit project");
        mp.setAlias("some-edit-project");
        mp.setBranch("some-branch");
        mp.setGoals("some,goals");
        mp.setPackagedPath("some/packaged");
        mp.setTargetPath("/some/target");
        mp.setUrl("some-url");
        mockMvc.perform(put(client, projectOne, mp))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void editAsAnonymous() throws Exception {
        log.info("Test edit project as anonymous");
        MProject mp = new MProject();
        mp.setName("Some edit project");
        mp.setAlias("some-edit-project");
        mp.setBranch("some-branch");
        mp.setGoals("some,goals");
        mp.setPackagedPath("some/packaged");
        mp.setTargetPath("/some/target");
        mp.setUrl("some-url");
        mockMvc.perform(put(null, projectOne, mp))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void deleteAsRoot() throws Exception {
        log.info("Test delete project as root");
        mockMvc.perform(delete(root, projectOne))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void deleteAsAdmin() throws Exception {
        log.info("Test delete project as admin");
        mockMvc.perform(delete(admin, projectOne))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void deleteAsClient() throws Exception {
        log.info("Test delete project as client");
        mockMvc.perform(delete(client, projectOne))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void deleteAsAnonymous() throws Exception {
        log.info("Test delete project as anonymous");
        mockMvc.perform(delete(null, projectOne))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void getBuildsAsRoot() throws Exception {
        log.info("Test get project's builds as root");
        mockMvc.perform(getBuilds(root, projectTwo))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getBuildsAsAdmin() throws Exception {
        log.info("Test get project's builds as admin");
        mockMvc.perform(getBuilds(admin, projectTwo))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getBuildsAsClient() throws Exception {
        log.info("Test get project's builds as client");
        mockMvc.perform(getBuilds(client, projectTwo))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void getBuildsAsAnonymous() throws Exception {
        log.info("Test get project's builds as anonymous");
        mockMvc.perform(getBuilds(null, projectTwo))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void getLastBuildAsRoot() throws Exception {
        log.info("Test get project's last builds as root");
        mockMvc.perform(getLastBuild(root, projectTwo))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getLastBuildAsAdmin() throws Exception {
        log.info("Test get project's last builds as admin");
        mockMvc.perform(getLastBuild(admin, projectTwo))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getLastBuildAsClient() throws Exception {
        log.info("Test get project's last builds as client");
        mockMvc.perform(getLastBuild(client, projectTwo))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void getLastBuildAsAnonymous() throws Exception {
        log.info("Test get project's last builds as anonymous");
        mockMvc.perform(getLastBuild(null, projectTwo))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void buildAsRoot() throws Exception {
        log.info("Test build project as root");
        mockMvc.perform(build(root, projectOne, null))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void buildAsAdmin() throws Exception {
        log.info("Test build project as admin");
        mockMvc.perform(build(admin, projectOne, null))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void buildAsClientForAll() throws Exception {
        log.info("Test build project as client for all");
        mockMvc.perform(build(client, projectOne, null))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void buildAsClientForSelf() throws Exception {
        log.info("Test build project as client for self");
        mockMvc.perform(build(client, projectOne, projectOne))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void buildAsClientForOther() throws Exception {
        log.info("Test build project as client for other");
        mockMvc.perform(build(client, projectOne, projectTwo))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    private MockHttpServletRequestBuilder search(User who) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .get("/projects")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder create(User who, MProject toCreate) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .post("/projects")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(toCreate));
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder get(User who, Project toGet) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .get("/projects/{projectId}", toGet.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder put(User who, Project toEdit, MProject edition) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .put("/projects/{projectId}", toEdit.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(edition));
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder delete(User who, Project toDelete) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .delete("/projects/{projectId}", toDelete.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder getBuilds(User who, Project toGet) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .get("/projects/{projectId}/builds", toGet.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder getLastBuild(User who, Project toGet) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .get("/projects/{projectId}/builds/last", toGet.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder build(User who, Project toBuild, Project privilege) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .post("/projects/{projectId}/builds", toBuild.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);
        userIntegrationConfig.authenticate(request, who, privilege);
        return request;
    }

}
