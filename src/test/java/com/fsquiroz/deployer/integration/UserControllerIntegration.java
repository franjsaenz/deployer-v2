package com.fsquiroz.deployer.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fsquiroz.deployer.config.UserIntegrationConfig;
import com.fsquiroz.deployer.entity.db.Role;
import com.fsquiroz.deployer.entity.db.User;
import com.fsquiroz.deployer.entity.json.MResetPassword;
import com.fsquiroz.deployer.entity.json.MUser;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-test.properties")
@ActiveProfiles("test")
@Slf4j
public class UserControllerIntegration {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private UserIntegrationConfig userIntegrationConfig;

    private User root;

    private User admin;

    private User client;

    @BeforeEach
    public void setup() {
        userIntegrationConfig.setup();
        this.root = userIntegrationConfig.getRoot();
        this.admin = userIntegrationConfig.getAdmin();
        this.client = userIntegrationConfig.getClient();
    }

    @AfterEach
    public void cleanup() {
        this.userIntegrationConfig.cleanup();
    }

    @Test
    public void searchAsRoot() throws Exception {
        log.info("Test search users as root");
        mockMvc.perform(search(root))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void searchAsAdmin() throws Exception {
        log.info("Test search users as admin");
        mockMvc.perform(search(admin))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void searchAsClient() throws Exception {
        log.info("Test search users as client");
        mockMvc.perform(search(client))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void searchAsAnonymous() throws Exception {
        log.info("Test search users as anonymous");
        mockMvc.perform(search(null))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void createAsRoot() throws Exception {
        log.info("Test create user as root");
        MUser mu = new MUser();
        mu.setRole(Role.ADMIN);
        mu.setEmail("some@test.com");
        mu.setFullName("Some Test");
        mu.setPassword("123456");
        mockMvc.perform(create(root, mu))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void createAsAdmin() throws Exception {
        log.info("Test create user as admin");
        MUser mu = new MUser();
        mu.setRole(Role.ADMIN);
        mu.setEmail("some@test.com");
        mu.setFullName("Some Test");
        mu.setPassword("123456");
        mockMvc.perform(create(admin, mu))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void createAsClient() throws Exception {
        log.info("Test create user as client");
        MUser mu = new MUser();
        mu.setRole(Role.ADMIN);
        mu.setEmail("some@test.com");
        mu.setFullName("Some Test");
        mu.setPassword("123456");
        mockMvc.perform(create(client, mu))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void createAsAnonymous() throws Exception {
        log.info("Test create user as anonymous");
        MUser mu = new MUser();
        mu.setRole(Role.ADMIN);
        mu.setEmail("some@test.com");
        mu.setFullName("Some Test");
        mu.setPassword("123456");
        mockMvc.perform(create(null, mu))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void getSelfAsRoot() throws Exception {
        log.info("Test get self user as root");
        mockMvc.perform(getSelf(root))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getSelfAsAdmin() throws Exception {
        log.info("Test get self user as admin");
        mockMvc.perform(getSelf(admin))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getSelfAsClient() throws Exception {
        log.info("Test get self user as client");
        mockMvc.perform(getSelf(client))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getOtherAsRoot() throws Exception {
        log.info("Test get other user as root");
        mockMvc.perform(get(root, client))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getOtherAsAdmin() throws Exception {
        log.info("Test get other user as admin");
        mockMvc.perform(get(admin, root))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void getOtherAsClient() throws Exception {
        log.info("Test get other user as client");
        mockMvc.perform(get(client, root))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void getOtherAsAnonymous() throws Exception {
        log.info("Test get other user as anonymous");
        mockMvc.perform(get(null, root))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void editSelfAsRoot() throws Exception {
        log.info("Test edit self user as root");
        MUser edition = new MUser();
        edition.setFullName("Root New Name");
        mockMvc.perform(putSelf(root, edition))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void editSelfAsAdmin() throws Exception {
        log.info("Test edit self user as admin");
        MUser edition = new MUser();
        edition.setFullName("Admin New Name");
        mockMvc.perform(putSelf(admin, edition))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void editSelfAsClient() throws Exception {
        log.info("Test edit self user as client");
        MUser edition = new MUser();
        edition.setFullName("Client New Name");
        mockMvc.perform(putSelf(client, edition))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void editOtherAsRoot() throws Exception {
        log.info("Test edit other user as root");
        MUser edition = new MUser();
        edition.setFullName("Admin New Name");
        mockMvc.perform(put(root, admin, edition))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void editOtherAsAdmin() throws Exception {
        log.info("Test edit other user as admin");
        MUser edition = new MUser();
        edition.setFullName("Root New Name");
        mockMvc.perform(put(admin, root, edition))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void editOtherAsClient() throws Exception {
        log.info("Test edit other user as client");
        MUser edition = new MUser();
        edition.setFullName("Admin New Name");
        mockMvc.perform(put(client, admin, edition))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void editOtherAsAnonymous() throws Exception {
        log.info("Test edit other user as anonymous");
        MUser edition = new MUser();
        edition.setFullName("Admin New Name");
        mockMvc.perform(put(null, admin, edition))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void deleteSelfAsRoot() throws Exception {
        log.info("Test delete self user as root");
        mockMvc.perform(deleteSelf(root))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void deleteSelfAsAdmin() throws Exception {
        log.info("Test delete self user as admin");
        mockMvc.perform(deleteSelf(admin))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void deleteSelfAsClient() throws Exception {
        log.info("Test delete self user as client");
        mockMvc.perform(deleteSelf(client))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void deleteOtherAsRoot() throws Exception {
        log.info("Test delete other user as root");
        mockMvc.perform(delete(root, admin))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void deleteOtherAsAdmin() throws Exception {
        log.info("Test delete other user as admin");
        mockMvc.perform(delete(admin, client))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void deleteOtherAsClient() throws Exception {
        log.info("Test delete other user as client");
        mockMvc.perform(delete(client, admin))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void deleteOtherAsAnonymous() throws Exception {
        log.info("Test delete other user as anonymous");
        mockMvc.perform(delete(null, admin))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void updateSelfPasswordAsRoot() throws Exception {
        log.info("Test update self password as root");
        MResetPassword pwd = new MResetPassword();
        pwd.setPassword("newPass");
        mockMvc.perform(updateSelfPassword(root, pwd))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void updateSelfPasswordAsAdmin() throws Exception {
        log.info("Test update self password as admin");
        MResetPassword pwd = new MResetPassword();
        pwd.setPassword("newPass");
        mockMvc.perform(updateSelfPassword(admin, pwd))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void updateSelfPasswordAsClient() throws Exception {
        log.info("Test update self password as root");
        MResetPassword pwd = new MResetPassword();
        pwd.setPassword("newPass");
        mockMvc.perform(updateSelfPassword(client, pwd))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void updateAdminPasswordAsRoot() throws Exception {
        log.info("Test update admin password as root");
        MResetPassword pwd = new MResetPassword();
        pwd.setPassword("newPass");
        mockMvc.perform(updatePassword(root, admin, pwd))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void updateClientPasswordAsRoot() throws Exception {
        log.info("Test update client password as root");
        MResetPassword pwd = new MResetPassword();
        pwd.setPassword("newPass");
        mockMvc.perform(updatePassword(root, client, pwd))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void updateRootPasswordAsAdmin() throws Exception {
        log.info("Test update root password as admin");
        MResetPassword pwd = new MResetPassword();
        pwd.setPassword("newPass");
        mockMvc.perform(updatePassword(admin, root, pwd))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void updateRootPasswordAsClient() throws Exception {
        log.info("Test update root password as client");
        MResetPassword pwd = new MResetPassword();
        pwd.setPassword("newPass");
        mockMvc.perform(updatePassword(client, root, pwd))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void updateRootPasswordAsAnonymous() throws Exception {
        log.info("Test update root password as anonymous");
        MResetPassword pwd = new MResetPassword();
        pwd.setPassword("newPass");
        mockMvc.perform(updatePassword(null, root, pwd))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void getSelfTokenAsRoot() throws Exception {
        log.info("Test get self token as root");
        mockMvc.perform(getSelfToken(root))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void getSelfTokenAsAdmin() throws Exception {
        log.info("Test get self token as admin");
        mockMvc.perform(getSelfToken(admin))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void getSelfTokenAsClient() throws Exception {
        log.info("Test get self token as client");
        mockMvc.perform(getSelfToken(client))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void getOtherTokenAsRoot() throws Exception {
        log.info("Test get other token as root");
        mockMvc.perform(getToken(root, admin))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void getOtherTokenAsAdmin() throws Exception {
        log.info("Test get other token as admin");
        mockMvc.perform(getToken(admin, root))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void getOtherTokenAsClient() throws Exception {
        log.info("Test get other token as client");
        mockMvc.perform(getToken(client, admin))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void getOtherTokenAsAnonymous() throws Exception {
        log.info("Test get other token as anonymous");
        mockMvc.perform(getToken(null, admin))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void getClientTokenAsRoot() throws Exception {
        log.info("Test get client token as root");
        mockMvc.perform(getToken(root, client))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getClientTokenAsAdmin() throws Exception {
        log.info("Test get client token as admin");
        mockMvc.perform(getToken(admin, client))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getClientTokenAsClient() throws Exception {
        log.info("Test get client token as client");
        mockMvc.perform(getToken(client, client))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void getClientTokenAsAnonymous() throws Exception {
        log.info("Test get client token as anonymous");
        mockMvc.perform(getToken(null, client))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    private MockHttpServletRequestBuilder search(User who) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .get("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder create(User who, MUser toCreate) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .post("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(toCreate));
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder getSelf(User who) throws Exception {
        return get(who, who);
    }

    private MockHttpServletRequestBuilder get(User who, User toGet) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .get("/users/{userId}", toGet.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder putSelf(User who, MUser edition) throws Exception {
        return put(who, who, edition);
    }

    private MockHttpServletRequestBuilder put(User who, User toEdit, MUser edition) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .put("/users/{userId}", toEdit.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(edition));
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder deleteSelf(User who) throws Exception {
        return delete(who, who);
    }

    private MockHttpServletRequestBuilder delete(User who, User toDelete) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .delete("/users/{userId}", toDelete.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder updateSelfPassword(User who, MResetPassword password) throws Exception {
        return updatePassword(who, who, password);
    }

    private MockHttpServletRequestBuilder updatePassword(User who, User toEdit, MResetPassword password) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .put("/users/{userId}/password", toEdit.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(password));
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder getSelfToken(User who) throws Exception {
        return getToken(who, who);
    }

    private MockHttpServletRequestBuilder getToken(User who, User toGet) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .get("/users/{userId}/token", toGet.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

}
