package com.fsquiroz.deployer.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fsquiroz.deployer.config.SshKeyIntegrationConfig;
import com.fsquiroz.deployer.config.UserIntegrationConfig;
import com.fsquiroz.deployer.entity.db.SshKeyPair;
import com.fsquiroz.deployer.entity.db.User;
import com.fsquiroz.deployer.entity.json.MSshKeyPair;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-test.properties")
@ActiveProfiles("test")
@Slf4j
public class SshKeyPairControllerIntegration {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private UserIntegrationConfig userIntegrationConfig;

    @Autowired
    private SshKeyIntegrationConfig sshKeyIntegrationConfig;

    private User root;

    private User admin;

    private User client;

    private SshKeyPair key;

    @BeforeEach
    public void setup() {
        userIntegrationConfig.setup();
        sshKeyIntegrationConfig.setup();
        this.root = userIntegrationConfig.getRoot();
        this.admin = userIntegrationConfig.getAdmin();
        this.client = userIntegrationConfig.getClient();
        this.key = sshKeyIntegrationConfig.getKey();
    }

    @AfterEach
    public void cleanup() {
        this.userIntegrationConfig.cleanup();
        this.sshKeyIntegrationConfig.cleanup();
    }

    @Test
    public void searchAsRoot() throws Exception {
        log.info("Test search keys as root");
        mockMvc.perform(search(root))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void searchAsAdmin() throws Exception {
        log.info("Test search keys as admin");
        mockMvc.perform(search(admin))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void searchAsClient() throws Exception {
        log.info("Test search keys as client");
        mockMvc.perform(search(client))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void searchAsAnonymous() throws Exception {
        log.info("Test search keys as anonymous");
        mockMvc.perform(search(null))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void createAsRoot() throws Exception {
        log.info("Test create key as root");
        MSshKeyPair mk = new MSshKeyPair();
        mk.setAlias("new-test-key");
        mockMvc.perform(create(root, mk))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void createAsAdmin() throws Exception {
        log.info("Test create key as admin");
        MSshKeyPair mk = new MSshKeyPair();
        mk.setAlias("new-test-key");
        mockMvc.perform(create(admin, mk))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void createAsClient() throws Exception {
        log.info("Test create key as client");
        MSshKeyPair mk = new MSshKeyPair();
        mk.setAlias("new-test-key");
        mockMvc.perform(create(client, mk))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void createAsAnonymous() throws Exception {
        log.info("Test create key as anonymous");
        MSshKeyPair mk = new MSshKeyPair();
        mk.setAlias("new-test-key");
        mockMvc.perform(create(null, mk))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void getAsRoot() throws Exception {
        log.info("Test get key as root");
        mockMvc.perform(get(root, key))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getAsAdmin() throws Exception {
        log.info("Test get key as admin");
        mockMvc.perform(get(admin, key))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getAsClient() throws Exception {
        log.info("Test get key as client");
        mockMvc.perform(get(client, key))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void getAsAnonymous() throws Exception {
        log.info("Test get key as anonymous");
        mockMvc.perform(get(null, key))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void deleteAsRoot() throws Exception {
        log.info("Test delete key as root");
        mockMvc.perform(delete(root, key))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void deleteAsAdmin() throws Exception {
        log.info("Test delete key as admin");
        mockMvc.perform(delete(admin, key))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void deleteAsClient() throws Exception {
        log.info("Test delete key as client");
        mockMvc.perform(delete(client, key))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void deleteAsAnonymous() throws Exception {
        log.info("Test delete key as anonymous");
        mockMvc.perform(delete(null, key))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    private MockHttpServletRequestBuilder search(User who) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .get("/keys")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder create(User who, MSshKeyPair toCreate) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .post("/keys")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(toCreate));
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder get(User who, SshKeyPair toGet) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .get("/keys/{keyId}", toGet.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder delete(User who, SshKeyPair toDelete) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .delete("/keys/{keyId}", toDelete.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

}
