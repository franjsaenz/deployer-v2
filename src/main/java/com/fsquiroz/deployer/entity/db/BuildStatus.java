package com.fsquiroz.deployer.entity.db;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum BuildStatus {

    OK("OK"),
    ERROR("ERROR");

    private final String name;

}
