package com.fsquiroz.deployer.entity.db;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum Role {

    SUPER("SUPER"),
    ADMIN("ADMIN"),
    CLIENT("CLIENT");

    private final String name;

}
