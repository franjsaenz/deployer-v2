package com.fsquiroz.deployer.entity.db;

import lombok.Data;

import java.io.Serializable;

@Data
public abstract class Entity implements Comparable<Entity>, Serializable {

    public abstract Long getId();

    @Override
    public int compareTo(Entity o) {
        Long id = getId();
        if (o == null || o.getId() == null) {
            return 1;
        } else if (id == null) {
            return -1;
        } else {
            return id.compareTo(o.getId());
        }
    }

}