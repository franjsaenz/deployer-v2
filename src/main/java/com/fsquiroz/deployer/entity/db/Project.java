package com.fsquiroz.deployer.entity.db;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import jakarta.persistence.*;
import java.util.Date;

@jakarta.persistence.Entity
@Table(name = "PROJECT")
@Getter
@Setter
@ToString(callSuper = true)
@EqualsAndHashCode(of = "id", callSuper = false)
public class Project extends Entity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATED")
    private Date created;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "UPDATED")
    private Date updated;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DELETED")
    private Date deleted;

    @Column(name = "NAME", columnDefinition = "TEXT")
    private String name;

    @Column(name = "ALIAS", columnDefinition = "TEXT")
    private String alias;

    @Column(name = "URL", columnDefinition = "TEXT")
    private String url;

    @Column(name = "BRANCH", columnDefinition = "TEXT")
    private String branch;

    @Column(name = "GOALS", columnDefinition = "TEXT")
    private String goals;

    @Column(name = "PACKAGED_PATH", columnDefinition = "TEXT")
    private String packagedPath;

    @Column(name = "TARGET_PATH", columnDefinition = "TEXT")
    private String targetPath;

    @ManyToOne
    @JoinColumn
    private SshKeyPair key;

}
