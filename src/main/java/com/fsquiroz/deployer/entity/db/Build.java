package com.fsquiroz.deployer.entity.db;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import jakarta.persistence.*;
import java.util.Date;

@jakarta.persistence.Entity
@Table(name = "BUILD")
@Getter
@Setter
@ToString(callSuper = true)
@EqualsAndHashCode(of = "id", callSuper = false)
public class Build extends Entity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATED")
    private Date created;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "UPDATED")
    private Date updated;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DELETED")
    private Date deleted;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "START")
    private Date start;

    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date end;

    @Column(name = "STATUS")
    @Enumerated(EnumType.STRING)
    private BuildStatus status;

    @Column(name = "MESSAGE", columnDefinition = "TEXT")
    private String message;

    @ManyToOne
    @JoinColumn
    private Project project;

}
