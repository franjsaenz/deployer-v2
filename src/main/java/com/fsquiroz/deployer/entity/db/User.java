package com.fsquiroz.deployer.entity.db;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import jakarta.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@jakarta.persistence.Entity
@Table(name = "USERS")
@Getter
@Setter
@ToString(callSuper = true)
@EqualsAndHashCode(of = "id", callSuper = false)
public class User extends Entity implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATED")
    private Date created;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "UPDATED")
    private Date updated;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DELETED")
    private Date deleted;

    @Column(name = "ROLE")
    @Enumerated(EnumType.STRING)
    private Role role;

    @Column(name = "FULL_NAME", columnDefinition = "TEXT")
    private String fullName;

    @Column(name = "EMAIL", columnDefinition = "TEXT")
    private String email;

    @Column(name = "PASSWORD", columnDefinition = "TEXT")
    private String password;

    @Transient
    private String token;

    @Transient
    private Long projectId;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<SimpleGrantedAuthority> authorities = new ArrayList<>();
        if (role != null) {
            authorities.add(new SimpleGrantedAuthority("ROLE_" + role.getName()));
        }
        return authorities;
    }

    @Override
    public String getUsername() {
        return this.email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return this.deleted == null;
    }

    @Override
    public boolean isAccountNonLocked() {
        return this.deleted == null;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return this.deleted == null;
    }

    @Override
    public boolean isEnabled() {
        return this.deleted == null;
    }

}
