package com.fsquiroz.deployer.entity.db;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import jakarta.persistence.*;
import java.util.Date;

@jakarta.persistence.Entity
@Table(name = "SETTINGS")
@Getter
@Setter
@ToString(callSuper = true)
@EqualsAndHashCode(of = "id", callSuper = false)
public class Settings extends Entity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATED")
    private Date created;

    @Column(name = "TIMESTAMP")
    private long timestamp;

    @Column(name = "JAVA_HOME", columnDefinition = "TEXT")
    private String javaHome;

    @Column(name = "MAVEN_HOME", columnDefinition = "TEXT")
    private String mavenHome;

}
