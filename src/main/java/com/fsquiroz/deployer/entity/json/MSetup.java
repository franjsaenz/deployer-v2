package com.fsquiroz.deployer.entity.json;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MSetup {

    private String fullName;

    private String email;

    private String javaHome;

    private String mavenHome;

}
