package com.fsquiroz.deployer.entity.json;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MResponse {

    private Date timestamp;

    private String message;

    public static MResponse of(String message) {
        Date now = new Date();
        return new MResponse(now, message);
    }

}
