package com.fsquiroz.deployer.entity.json;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MProject {

    private Long id;

    private Date created;

    private Date updated;

    private Date deleted;

    private String name;

    private String alias;

    private String url;

    private String branch;

    private String goals;

    private String packagedPath;

    private String targetPath;

    private MSshKeyPair key;

}
