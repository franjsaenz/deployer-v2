package com.fsquiroz.deployer.entity.json;

import com.fsquiroz.deployer.entity.db.Role;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MUser {

    private Long id;

    private Date created;

    private Date updated;

    private Date deleted;

    private Role role;

    private String fullName;

    private String email;

    private String password;

    private String token;

}
