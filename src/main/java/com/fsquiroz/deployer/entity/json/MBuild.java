package com.fsquiroz.deployer.entity.json;

import com.fsquiroz.deployer.entity.db.BuildStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MBuild {

    private Long id;

    private Date created;

    private Date updated;

    private Date deleted;

    private Date start;

    private Date end;

    private BuildStatus status;

    private String message;

    private Long projectId;

    private String projectName;

}
