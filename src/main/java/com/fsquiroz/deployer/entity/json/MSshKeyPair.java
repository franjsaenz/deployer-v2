package com.fsquiroz.deployer.entity.json;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MSshKeyPair {

    private Long id;

    private Date created;

    private Date updated;

    private Date deleted;

    private String publicKey;

    private String alias;

}
