package com.fsquiroz.deployer.security;

import com.fsquiroz.deployer.entity.db.Project;
import com.fsquiroz.deployer.entity.db.User;
import com.fsquiroz.deployer.exception.NotFoundException;
import com.fsquiroz.deployer.exception.UnauthorizedException;
import com.fsquiroz.deployer.repository.UserRepository;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.util.Date;

import static java.util.Optional.ofNullable;

@Component
public class SecurityCheck {

    private final UserRepository userRepository;

    private final SecretKey jwtSecret;

    public SecurityCheck(
            UserRepository userRepository,
            @Value("${security.jwt.secret}") final String jwtSecret,
            @Value("${security.jwt.alg}") final String jwtAlg
    ) {
        this.userRepository = userRepository;
        this.jwtSecret = getKey(jwtSecret, jwtAlg);
    }

    private SecretKey getKey(String jwtSecret, String algorithm) {
        byte[] bytes = Base64.decodeBase64(jwtSecret);
        return new SecretKeySpec(bytes, algorithm);
    }

    public User get(Long id) {
        User u = userRepository.findById(id).orElseThrow(() -> new NotFoundException("Unable to find user"));
        if (u.getDeleted() != null) {
            throw new UnauthorizedException("This account has been suspended");
        }
        return u;
    }

    public User authenticate(String token) {
        Claims claims = Jwts.parser()
                .verifyWith(jwtSecret)
                .build()
                .parseSignedClaims(token)
                .getPayload();
        Long id = claims.get("userId", Long.class);
        User u = get(id);
        Long projectId = claims.get("projectId", Long.class);
        u.setProjectId(projectId);
        return u;
    }

    public String generateToken(User issuer, Date expiration, Date notBefore, User authorized, Project project) {
        return Jwts.builder()
                .claims()
                .subject(authorized.getEmail())
                .issuer(issuer != null ? issuer.getEmail() : null)
                .issuedAt(new Date())
                .add("userId", authorized.getId())
                .add("userName", authorized.getFullName())
                .add("projectId", project != null ? project.getId() : null)
                .expiration(expiration)
                .notBefore(notBefore)
                .and()
                .signWith(jwtSecret)
                .compact();
    }

    public User getAuthentication() {
        return ofNullable(SecurityContextHolder.getContext().getAuthentication())
                .map(Authentication::getPrincipal)
                .map(principal -> principal instanceof User u ? u : null)
                .orElseThrow(() -> new UnauthorizedException("Insufficient permissions"));
    }

}
