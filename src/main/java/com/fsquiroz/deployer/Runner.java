package com.fsquiroz.deployer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.io.File;

@Configuration
@Slf4j
@Profile("!test")
public class Runner implements ApplicationRunner {

    @Value("${com.fsquiroz.deployer.sshkeys.path}")
    private String path;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        log.info("Starting Application Runner");
        validateSshFolder();
    }

    private void validateSshFolder() {
        if (this.path == null || this.path.isEmpty()) {
            log.warn("Unable to determinate ssh folder's path");
            return;
        }
        File f = new File(this.path);
        if (!f.exists()) {
            f.mkdirs();
        }
    }

}
