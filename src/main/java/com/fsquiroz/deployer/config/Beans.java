package com.fsquiroz.deployer.config;

import org.jasypt.util.password.StrongPasswordEncryptor;
import org.springframework.boot.actuate.web.exchanges.HttpExchangeRepository;
import org.springframework.boot.actuate.web.exchanges.InMemoryHttpExchangeRepository;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.*;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@Profile("!test")
@EnableScheduling
@PropertySources({
        @PropertySource(value = "classpath:application.properties")
        ,
        @PropertySource(value = "file:${user.home}${file.separator}.deployer${file.separator}application.properties")
})
public class Beans {

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new PasswordEncoder() {
            private final StrongPasswordEncryptor spe = new StrongPasswordEncryptor();

            @Override
            public String encode(CharSequence rawPassword) {
                return spe.encryptPassword(rawPassword.toString());
            }

            @Override
            public boolean matches(CharSequence rawPassword, String encodedPassword) {
                return spe.checkPassword(rawPassword.toString(), encodedPassword);
            }
        };
    }

    @Bean
    public ServletRegistrationBean<LoggerDispatcherServlet> dispatcherRegistration(
            LoggerDispatcherServlet loggerDispatcherServlet) {
        return new ServletRegistrationBean<>(loggerDispatcherServlet);
    }

    @Bean
    public HttpExchangeRepository httpExchangeRepository() {
        var repo = new InMemoryHttpExchangeRepository();
        repo.setCapacity(500);
        return repo;
    }
}
