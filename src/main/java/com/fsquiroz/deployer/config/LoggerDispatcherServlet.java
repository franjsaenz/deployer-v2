package com.fsquiroz.deployer.config;

import com.fsquiroz.deployer.entity.db.User;
import com.fsquiroz.deployer.security.SecurityCheck;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.HandlerExecutionChain;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@Slf4j
@Component
@RequiredArgsConstructor
public class LoggerDispatcherServlet extends DispatcherServlet {

    private final SecurityCheck securityCheck;

    @Override
    protected void doDispatch(HttpServletRequest request, HttpServletResponse response) throws Exception {
        long start = System.currentTimeMillis();
        HandlerExecutionChain handler = getHandler(request);
        try {
            super.doDispatch(request, response);
        } finally {
            if (handler.toString().contains("fsquiroz")) {
                long end = System.currentTimeMillis();
                debug(request, response, handler, end - start);
            }
        }
    }

    private void debug(HttpServletRequest request,
                       HttpServletResponse response,
                       HandlerExecutionChain handler,
                       long elapsed) {
        User u;
        try {
            u = securityCheck.getAuthentication();
        } catch (Exception e) {
            u = null;
        }
        String user;
        String role;
        if (u != null) {
            user = u.getFullName() + " (" + u.getEmail() + ")";
        } else {
            user = "ANONYMOUS";
        }
        if (u != null && u.getRole() != null) {
            role = u.getRole().getName();
        } else {
            role = "ANONYMOUS";
        }
        log.debug("\n"
                        + "HTTP Request:\n"
                        + "\turl: {}\n"
                        + "\tmethod: {}\n"
                        + "\tclient: {}\n"
                        + "\tcredential: {}\n"
                        + "\tuser: {}\n"
                        + "\trole: {}\n"
                        + "Handler:\n"
                        + "\tmethod: {}\n"
                        + "\ttook: {}ms\n"
                        + "HTTP Response:\n"
                        + "\tstatus: {}\n",
                request.getRequestURL(),
                request.getMethod(),
                request.getRemoteAddr(),
                u != null ? u.getId() : "ANONYMOUS",
                user,
                role,
                handler.toString(),
                elapsed,
                response.getStatus());
    }

}
