package com.fsquiroz.deployer.service.declaration;

public interface ISchedulerService {

    void addExecutor(Runnable executor);

}
