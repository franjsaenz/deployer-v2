package com.fsquiroz.deployer.service.declaration;

import com.fsquiroz.deployer.entity.db.Project;

public interface IGitService {

    void clone(Project p);

    void update(Project p);

    void checkout(Project p);

    void reset(Project p);

    void pull(Project p);

}
