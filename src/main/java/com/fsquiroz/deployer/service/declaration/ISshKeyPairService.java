package com.fsquiroz.deployer.service.declaration;

import com.fsquiroz.deployer.entity.db.SshKeyPair;
import com.fsquiroz.deployer.entity.json.MSshKeyPair;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;

public interface ISshKeyPairService extends IBuildiableService<SshKeyPair, MSshKeyPair> {

    @PreAuthorize("hasAnyRole('SUPER', 'ADMIN')")
    SshKeyPair create(MSshKeyPair model);

    @PreAuthorize("hasAnyRole('SUPER', 'ADMIN')")
    SshKeyPair update(SshKeyPair original, MSshKeyPair edit);

    @PreAuthorize("hasRole('SUPER')")
    SshKeyPair delete(SshKeyPair entity);

    SshKeyPair get(Long id);

    String getKeyPath(SshKeyPair skp);

    @PreAuthorize("hasAnyRole('SUPER', 'ADMIN')")
    Page<SshKeyPair> search(String term, Pageable pageable);

    MSshKeyPair build(SshKeyPair domain, boolean withPublicKey);

    default MSshKeyPair build(SshKeyPair domain) {
        return build(domain, false);
    }

}
