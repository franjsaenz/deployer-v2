package com.fsquiroz.deployer.service.declaration;

import com.fsquiroz.deployer.entity.db.Settings;
import com.fsquiroz.deployer.entity.json.MSettings;

public interface ISettingsService extends IBuildiableService<Settings, MSettings> {

    Settings getLast();

    Settings update(MSettings ms);

}
