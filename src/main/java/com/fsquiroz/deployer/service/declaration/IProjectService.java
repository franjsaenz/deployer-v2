package com.fsquiroz.deployer.service.declaration;

import com.fasterxml.jackson.databind.JsonNode;
import com.fsquiroz.deployer.entity.db.Project;
import com.fsquiroz.deployer.entity.db.SshKeyPair;
import com.fsquiroz.deployer.entity.db.User;
import com.fsquiroz.deployer.entity.json.MProject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;

public interface IProjectService extends IBuildiableService<Project, MProject> {

    @PreAuthorize("hasAnyRole('SUPER', 'ADMIN')")
    Project create(MProject model, SshKeyPair key);

    @PreAuthorize("hasAnyRole('SUPER', 'ADMIN')")
    Project update(Project original, MProject edit, SshKeyPair key);

    @PreAuthorize("hasRole('SUPER')")
    Project delete(Project entity);

    Project get(Long id);

    Page<Project> search(String term, Pageable pageable);

    @PreAuthorize("hasAnyRole('SUPER', 'ADMIN', 'CLIENT')")
    void triggerUpdate(User user, Project p, JsonNode json);

}
