package com.fsquiroz.deployer.service.declaration;

import com.fsquiroz.deployer.entity.db.Build;
import com.fsquiroz.deployer.entity.db.Project;
import com.fsquiroz.deployer.entity.json.MBuild;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface IBuildService extends IBuildiableService<Build, MBuild> {

    Build create(Project project);

    Build update(Build original, MBuild edit);

    Build get(Long id);

    Build getLast(Project project);

    Page<Build> search(Project project, Pageable pageable);

}
