package com.fsquiroz.deployer.service.declaration;

import com.fsquiroz.deployer.entity.db.Project;
import com.fsquiroz.deployer.entity.db.User;
import com.fsquiroz.deployer.entity.json.MLogin;
import com.fsquiroz.deployer.entity.json.MResetPassword;
import com.fsquiroz.deployer.entity.json.MSetup;
import com.fsquiroz.deployer.entity.json.MUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.Date;

public interface IUserService extends IBuildiableService<User, MUser> {

    User login(MLogin login, Date expiration, Date notBefore);

    User getByEmail(String email);

    @PreAuthorize("hasRole('SUPER')")
    User changePassword(User original, MResetPassword edit);

    @PreAuthorize("hasAnyRole('SUPER', 'ADMIN')")
    User generateToken(User user, Project project, Date expiration, Date notBefore);

    User setup(MSetup ms);

    @PreAuthorize("hasRole('SUPER')")
    User create(MUser model);

    @PreAuthorize("hasRole('SUPER') or (principal.id == #original.id and principal.role != 'CLIENT')")
    User update(User original, MUser edit);

    @PreAuthorize("hasRole('SUPER')")
    User delete(User original);

    @PostAuthorize("hasRole('SUPER') or (isAuthenticated() and principal.id == returnObject.id) or returnObject.role == T(com.fsquiroz.deployer.entity.db.Role).CLIENT")
    User get(Long id);

    @PreAuthorize("hasRole('SUPER')")
    Page<User> search(String term, Pageable pageable);

}
