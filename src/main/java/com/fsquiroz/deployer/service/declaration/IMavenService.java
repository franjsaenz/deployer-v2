package com.fsquiroz.deployer.service.declaration;

import com.fsquiroz.deployer.entity.db.Project;

import java.util.List;

public interface IMavenService {

    void validatePom(Project p);

    void execute(Project p, List<String> goals);

}
