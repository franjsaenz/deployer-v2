package com.fsquiroz.deployer.service;

import com.fsquiroz.deployer.entity.db.Project;
import com.fsquiroz.deployer.entity.db.Settings;
import com.fsquiroz.deployer.exception.BadRequestException;
import com.fsquiroz.deployer.exception.ServerException;
import com.fsquiroz.deployer.service.declaration.IMavenService;
import com.fsquiroz.deployer.service.declaration.ISettingsService;
import lombok.extern.slf4j.Slf4j;
import org.apache.maven.shared.invoker.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.List;

@Service
@Profile("!test")
@Slf4j
public class MavenService implements IMavenService {

    private String projectPath;

    private ISettingsService settingsService;

    public MavenService(
            ISettingsService settingsService,
            @Value("${com.fsquiroz.deployer.projects.path}") final String projectPath
    ) {
        this.settingsService = settingsService;
        this.projectPath = projectPath;
    }

    @Override
    public void validatePom(Project p) {
        File pom = getProjectPom(p);
        if (!pom.exists()) {
            throw new BadRequestException("Project does not have pom.xml file under '" + pom.getAbsolutePath() + "'");
        }
    }

    @Override
    public void execute(Project p, List<String> goals) {
        log.info("Executing maven on project id '{}' name '{}' with goals '{}'", p.getId(), p.getName(), goals);
        validatePom(p);
        Settings s = settingsService.getLast();
        InvocationRequest request = new DefaultInvocationRequest();
        request.setPomFile(getProjectPom(p));
        request.setGoals(goals);
        request.setJavaHome(new File(s.getJavaHome()));
        Invoker invoker = new DefaultInvoker();
        request.setBatchMode(true);
        invoker.setMavenHome(new File(s.getMavenHome()));
        try {
            InvocationResult result = invoker.execute(request);
            if (result.getExitCode() != 0) {
                throw new ServerException("Maven process exited with code " + result.getExitCode());
            }
        } catch (MavenInvocationException e) {
            throw new ServerException("Maven process could not be executed. Caused by: " + e.getMessage(), e);
        }
        log.info("Maven process finish");
    }

    private File getProjectPom(Project p) {
        return new File(this.projectPath + p.getId() + File.separatorChar + "pom.xml");
    }

}
