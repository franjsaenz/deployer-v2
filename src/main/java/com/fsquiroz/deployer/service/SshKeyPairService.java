package com.fsquiroz.deployer.service;

import com.fsquiroz.deployer.entity.db.Project;
import com.fsquiroz.deployer.entity.db.SshKeyPair;
import com.fsquiroz.deployer.entity.json.MSshKeyPair;
import com.fsquiroz.deployer.exception.BadRequestException;
import com.fsquiroz.deployer.exception.NotFoundException;
import com.fsquiroz.deployer.exception.ServerException;
import com.fsquiroz.deployer.repository.ProjectRepository;
import com.fsquiroz.deployer.repository.SshKeyPairRepository;
import com.fsquiroz.deployer.service.declaration.ISshKeyPairService;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.KeyPair;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Date;
import java.util.List;

@Service
@Profile("!test")
public class SshKeyPairService implements ISshKeyPairService {

    private String path;

    private ProjectRepository projectRepository;

    private SshKeyPairRepository sshKeyPairRepository;

    public SshKeyPairService(
            SshKeyPairRepository sshKeyPairRepository,
            ProjectRepository projectRepository,
            @Value("${com.fsquiroz.deployer.sshkeys.path}") final String path
    ) {
        this.sshKeyPairRepository = sshKeyPairRepository;
        this.projectRepository = projectRepository;
        this.path = path;
    }

    @Override
    public MSshKeyPair build(SshKeyPair domain, boolean withPublicKey) {
        MSshKeyPair ms = new MSshKeyPair();
        ms.setId(domain.getId());
        ms.setCreated(domain.getCreated());
        ms.setUpdated(domain.getUpdated());
        ms.setDeleted(domain.getDeleted());
        ms.setAlias(domain.getAlias());
        if (withPublicKey) {
            try {
                List<String> lines = Files.readAllLines(new File(getKeyPath(domain) + ".pub").toPath());
                String pk = "";
                for (String line : lines) {
                    if (!pk.isEmpty()) {
                        pk += "\n";
                    }
                    pk += line;
                }
                ms.setPublicKey(pk);
            } catch (IOException ioe) {
                throw new ServerException("There has been an unexpected error: " + ioe.getMessage(), ioe);
            }
        }
        return ms;
    }

    @Override
    public SshKeyPair create(MSshKeyPair model) {
        if (model.getAlias() == null || model.getAlias().isEmpty()) {
            throw new BadRequestException("Missing parameter 'alias'");
        }
        SshKeyPair ssh = new SshKeyPair();
        ssh.setCreated(new Date());
        ssh.setAlias(model.getAlias());
        ssh = sshKeyPairRepository.save(ssh);
        try {
            String path = getKeyPath(ssh);
            JSch jsch = new JSch();
            KeyPair kp = KeyPair.genKeyPair(jsch, KeyPair.RSA);
            kp.writePrivateKey(path);
            kp.writePublicKey(path + ".pub", ssh.getAlias());
            kp.dispose();
        } catch (JSchException | IOException e) {
            try {
                sshKeyPairRepository.delete(ssh);
            } finally {
                throw new ServerException("There has been an unexpected error: " + e.getMessage(), e);
            }
        }
        return ssh;
    }

    @Override
    public SshKeyPair update(SshKeyPair original, MSshKeyPair edit) {
        throw new BadRequestException("Ssh key entity can not be modify");
    }

    @Override
    public SshKeyPair delete(SshKeyPair entity) {
        Project p = projectRepository.findFirstByKey(entity);
        if (p != null) {
            throw new BadRequestException("Can not delete key id '" + entity.getId() + "'. Project id '" + p.getId() + "' is using it.");
        }
        if (entity.getDeleted() != null) {
            throw new BadRequestException("This ssh key has already been deleted");
        }
        entity.setDeleted(new Date());
        return sshKeyPairRepository.save(entity);
    }

    @Override
    public SshKeyPair get(Long id) {
        return sshKeyPairRepository.findById(id).orElseThrow(() -> new NotFoundException("Ssh key not found. Id " + id));
    }

    @Override
    public Page<SshKeyPair> search(String term, Pageable pageable) {
        if (term == null || term.isEmpty()) {
            return sshKeyPairRepository.findByDeletedIsNull(pageable);
        } else {
            return sshKeyPairRepository.searchKeys(term, pageable);
        }
    }

    @Override
    public String getKeyPath(SshKeyPair skp) {
        return this.path + skp.getId();
    }

}
