package com.fsquiroz.deployer.service;

import com.fsquiroz.deployer.service.declaration.ISchedulerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.concurrent.ConcurrentLinkedQueue;

@Service
@Slf4j
public class SchedulerService implements ISchedulerService {

    private ConcurrentLinkedQueue<Runnable> executors = new ConcurrentLinkedQueue<>();

    @Override
    public void addExecutor(Runnable executor) {
        this.executors.offer(executor);
    }

    @Scheduled(fixedDelay = 1000, initialDelay = 30000)
    public void run() {
        Runnable executor = this.executors.poll();
        if (executor == null) {
            return;
        }
        executor.run();
    }

}
