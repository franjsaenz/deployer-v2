package com.fsquiroz.deployer.service;

import com.fsquiroz.deployer.entity.db.Build;
import com.fsquiroz.deployer.entity.db.BuildStatus;
import com.fsquiroz.deployer.entity.db.Project;
import com.fsquiroz.deployer.entity.json.MBuild;
import com.fsquiroz.deployer.exception.BadRequestException;
import com.fsquiroz.deployer.exception.ServerException;
import com.fsquiroz.deployer.service.declaration.IBuildService;
import com.fsquiroz.deployer.service.declaration.IGitService;
import com.fsquiroz.deployer.service.declaration.IMavenService;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Slf4j
public class ProjectExecutor implements Runnable {

    private Project project;

    private Build build;

    private Map<Long, ProjectExecutor> executors;

    private IGitService gitService;

    private IMavenService mavenService;

    private IBuildService buildService;

    private String projectPath;

    public ProjectExecutor(Project project, Map<Long, ProjectExecutor> executors, IGitService gitService, IMavenService mavenService, IBuildService buildService, String projectPath) {
        this.project = project;
        this.executors = executors;
        this.gitService = gitService;
        this.mavenService = mavenService;
        this.buildService = buildService;
        this.projectPath = projectPath;
    }

    @Override
    public void run() {
        try {
            start();
            updateRepository();
            runMaven();
            deploy();
            finish();
        } catch (Exception e) {
            if (build != null) {
                MBuild mb = new MBuild();
                mb.setStatus(BuildStatus.ERROR);
                mb.setMessage(e.getMessage());
                mb.setEnd(new Date());
                buildService.update(build, mb);
            }
            log.error(e.getMessage(), e);
        } finally {
            this.executors.remove(project.getId());
        }
    }

    private void start() {
        log.info("Starting build process for project id '{}' name '{}'", project.getId(), project.getName());
        executors.put(project.getId(), this);
        this.build = buildService.create(project);
    }

    private void finish() {
        MBuild mb = new MBuild();
        mb.setStatus(BuildStatus.OK);
        mb.setMessage("Success");
        mb.setEnd(new Date());
        buildService.update(build, mb);
        log.info("Build process finished successfully. Took {}ms", (mb.getEnd().getTime() - build.getStart().getTime()));
    }

    private void updateRepository() {
        gitService.update(project);
    }

    private void runMaven() {
        List<String> goals = Arrays.asList(project.getGoals().split(","));
        mavenService.execute(project, goals);
    }

    private void deploy() {
        File packaged = new File(this.projectPath + File.separator + project.getId() + File.separator + project.getPackagedPath());
        File target = new File(project.getTargetPath());
        if (!packaged.exists()) {
            throw new BadRequestException("Unable to find packaged file");
        }
        log.info("Copying '{}' into '{}'", packaged.getAbsolutePath(), target.getAbsolutePath());
        try {
            Files.copy(packaged.toPath(), target.toPath(), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException ioe) {
            throw new ServerException("Unable to copy packaged file '" + packaged.getAbsolutePath() + "' into '" + target.getAbsolutePath() + "'. Caused by: " + ioe.getMessage(), ioe);
        }
    }

}
