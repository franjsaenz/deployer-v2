package com.fsquiroz.deployer.service;

import com.fsquiroz.deployer.entity.db.Project;
import com.fsquiroz.deployer.exception.ServerException;
import com.fsquiroz.deployer.service.declaration.IGitService;
import com.fsquiroz.deployer.service.declaration.ISshKeyPairService;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.jgit.api.*;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.internal.WorkQueue;
import org.eclipse.jgit.transport.*;
import org.eclipse.jgit.util.FS;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import jakarta.annotation.PreDestroy;
import java.io.File;
import java.io.IOException;

@Service
@Profile("!test")
@Slf4j
public class GitService implements IGitService {

    private String projectPath;

    private ISshKeyPairService sshKeyPairService;

    public GitService(
            ISshKeyPairService sshKeyPairService,
            @Value("${com.fsquiroz.deployer.projects.path}") final String projectPath
    ) {
        this.sshKeyPairService = sshKeyPairService;
        this.projectPath = projectPath;
    }

    @Override
    public void clone(Project p) {
        log.info("Cloning project id '{}' name '{}'", p.getId(), p.getName());
        File repo = getFile(p);
        CloneCommand cmd = Git.cloneRepository()
                .setDirectory(repo)
                .setURI(p.getUrl())
                .setBranch(p.getBranch());
        callSsh(cmd, p);
        log.info("Clone process finish");
    }

    @Override
    public void update(Project p) {
        log.info("Updating project id '{}' name '{}'", p.getId(), p.getName());
        reset(p);
        checkout(p);
        pull(p);
        log.info("Update process finish");
    }

    @Override
    public void checkout(Project p) {
        log.info("Checking out project id '{}' name '{}' to branch '{}'", p.getId(), p.getName(), p.getBranch());
        GitCommand cmd = openRepo(p)
                .checkout()
                .setName(p.getBranch());
        call(cmd);
        log.info("Checkout process finish");
    }

    @Override
    public void reset(Project p) {
        log.info("Resetting project id '{}' name '{}'", p.getId(), p.getName());
        GitCommand cmd = openRepo(p)
                .reset()
                .setMode(ResetCommand.ResetType.HARD);
        call(cmd);
        log.info("Reset process finish");
    }

    @Override
    public void pull(Project p) {
        log.info("Pulling project id '{}' name '{}' branch '{}'", p.getId(), p.getName(), p.getBranch());
        PullCommand cmd = openRepo(p)
                .pull()
                .setRemoteBranchName(p.getBranch());
        callSsh(cmd, p);
        log.info("Pull process finish");
    }

    private File getFile(Project p) {
        return new File(this.projectPath + p.getId());
    }

    private Git openRepo(Project p) {
        File repo = getFile(p);
        try {
            return Git.open(repo);
        } catch (IOException ioe) {
            throw new ServerException("There has been an unexpected error wile opening a git project. Caused by: " + ioe.getMessage(), ioe);
        }
    }

    private <T> T call(GitCommand cmd) {
        try {
            return (T) cmd.call();
        } catch (GitAPIException gae) {
            throw new ServerException("There has been an unexpected error wile executing a git command. Caused by: " + gae.getMessage(), gae);
        }
    }

    private <T> T callSsh(TransportCommand cmd, Project p) {
        File privateKey = null;
        if (p.getKey() != null) {
            String path = sshKeyPairService.getKeyPath(p.getKey());
            privateKey = new File(path);
        }
        SshTransportConfigCallback stcc = SshTransportConfigCallback.build(privateKey);
        cmd.setTransportConfigCallback(stcc);
        try {
            return (T) cmd.call();
        } catch (GitAPIException gae) {
            throw new ServerException("There has been an unexpected error wile running a git command. Caused by: " + gae.getMessage(), gae);
        }
    }

    @PreDestroy
    public void destroy() {
        WorkQueue.getExecutor().shutdownNow();
    }

    private static class SshTransportConfigCallback implements TransportConfigCallback {

        private final SshSessionFactory factory;

        private SshTransportConfigCallback(SshSessionFactory factory) {
            this.factory = factory;
        }

        @Override
        public void configure(Transport t) {
            SshTransport sshTransport = (SshTransport) t;
            sshTransport.setSshSessionFactory(factory);
        }

        public static SshTransportConfigCallback build(File privateKey) {
            InnerSshSessionFactory issf = new InnerSshSessionFactory(privateKey);
            return new SshTransportConfigCallback(issf);
        }

        private static class InnerSshSessionFactory extends JschConfigSessionFactory {

            private final File privateKey;

            private InnerSshSessionFactory(File privateKey) {
                this.privateKey = privateKey;
            }

            @Override
            protected void configure(OpenSshConfig.Host host, Session sn) {
                sn.setConfig("StrictHostKeyChecking", "no");
            }

            @Override
            protected JSch createDefaultJSch(FS fs) throws JSchException {
                JSch jsch = super.createDefaultJSch(fs);
                if (privateKey != null) {
                    jsch.addIdentity(this.privateKey.getAbsolutePath());
                } else {
                    jsch.removeAllIdentity();
                }
                return jsch;
            }

        }

    }

}
