package com.fsquiroz.deployer.service;

import com.fsquiroz.deployer.entity.db.Project;
import com.fsquiroz.deployer.entity.db.Role;
import com.fsquiroz.deployer.entity.db.User;
import com.fsquiroz.deployer.entity.json.*;
import com.fsquiroz.deployer.exception.BadRequestException;
import com.fsquiroz.deployer.exception.NotFoundException;
import com.fsquiroz.deployer.exception.UnauthorizedException;
import com.fsquiroz.deployer.repository.UserRepository;
import com.fsquiroz.deployer.security.SecurityCheck;
import com.fsquiroz.deployer.service.declaration.ISettingsService;
import com.fsquiroz.deployer.service.declaration.IUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.UUID;

@Service
@Profile("!test")
@Slf4j
public class UserService implements IUserService {

    private ISettingsService settingsService;

    private SecurityCheck securityCheck;

    private PasswordEncoder passwordEncoder;

    private UserRepository userRepository;

    public UserService(
            ISettingsService settingsService,
            SecurityCheck securityCheck,
            PasswordEncoder passwordEncoder,
            UserRepository userRepository
    ) {
        this.settingsService = settingsService;
        this.securityCheck = securityCheck;
        this.passwordEncoder = passwordEncoder;
        this.userRepository = userRepository;
    }

    @Override
    public User login(MLogin login, Date expiration, Date notBefore) {
        if (login.getEmail() == null || login.getEmail().isEmpty()) {
            throw new BadRequestException("Missing parameter 'email'");
        }
        if (login.getPassword() == null || login.getPassword().isEmpty()) {
            throw new BadRequestException("Missing parameter 'password'");
        }
        UnauthorizedException ue = new UnauthorizedException("Invalid email or password");
        User u;
        try {
            u = getByEmail(login.getEmail());
        } catch (NotFoundException nfe) {
            throw ue;
        }
        if (u.getDeleted() != null) {
            throw new UnauthorizedException("This account has been suspended");
        }
        if (u.getRole() == Role.CLIENT || u.getRole() == null) {
            throw new UnauthorizedException("This account does not have privileges to login");
        }
        if (!passwordEncoder.matches(login.getPassword(), u.getPassword())) {
            throw ue;
        }
        String token = securityCheck.generateToken(null, expiration, notBefore, u, null);
        u.setToken(token);
        return u;
    }

    @Override
    public User getByEmail(String email) {
        return userRepository.findByEmail(email).orElseThrow(() -> new NotFoundException("User not found. Email '" + email + "'"));
    }

    @Override
    public User changePassword(User original, MResetPassword edit) {
        if (original.getRole() == Role.CLIENT) {
            throw new BadRequestException("Users with role 'client' can not have password");
        }
        if (edit.getPassword() == null || edit.getPassword().isEmpty()) {
            throw new BadRequestException("Missing parameter 'password'");
        }
        original.setPassword(passwordEncoder.encode(edit.getPassword()));
        return userRepository.save(original);
    }

    @Override
    public User generateToken(User user, Project project, Date expiration, Date notBefore) {
        if (user.getRole() != Role.CLIENT) {
            throw new BadRequestException("Passwordless token can only be generated to users with role 'client'");
        }
        String token = securityCheck.generateToken(securityCheck.getAuthentication(), expiration, notBefore, user, project);
        user.setToken(token);
        return user;
    }

    @Override
    public User setup(MSetup ms) {
        if (ms.getEmail() == null || ms.getEmail().isEmpty()) {
            throw new BadRequestException("Missing parameter 'email'");
        }
        if (ms.getFullName() == null || ms.getFullName().isEmpty()) {
            throw new BadRequestException("Missing parameter 'fullName'");
        }
        User u = userRepository.findFirstByDeletedIsNullAndRoleOrderByCreatedDesc(Role.SUPER);
        if (u != null && !u.getEmail().equals(ms.getEmail())) {
            log.warn("There was an attempt to run the setup with an email '{}', when there already is an super user with email address '{}'", ms.getEmail(), u.getEmail());
            throw new UnauthorizedException("There is already an account with admin role super and with a different email address. Setup can only be performed when there is no user with role super, or for the first user with role super");
        }
        String pass = UUID.randomUUID().toString().replace("-", "");
        if (u == null) {
            if (ms.getJavaHome() == null || ms.getJavaHome().isEmpty()) {
                throw new BadRequestException("Missing parameter 'javaHome'");
            }
            if (ms.getMavenHome() == null | ms.getMavenHome().isEmpty()) {
                throw new BadRequestException("Missing parameter 'mavenHome'");
            }
            u = new User();
            u.setEmail(ms.getEmail());
            u.setRole(Role.SUPER);
            u.setCreated(new Date());
            MSettings st = new MSettings();
            st.setMavenHome(ms.getMavenHome());
            st.setJavaHome(ms.getJavaHome());
            settingsService.update(st);
        } else {
            u.setUpdated(new Date());
        }
        u.setFullName(ms.getFullName());
        u.setPassword(passwordEncoder.encode(pass));
        u = userRepository.save(u);
        log.warn("\nSuper user new account's password:\n\n{}\n\nWARNING!!!\nIt is highly advisable to change this password as soon as possible.\n\n", pass);
        return u;
    }

    @Override
    public MUser build(User domain) {
        MUser mu = new MUser();
        mu.setId(domain.getId());
        mu.setCreated(domain.getCreated());
        mu.setUpdated(domain.getUpdated());
        mu.setDeleted(domain.getDeleted());
        mu.setEmail(domain.getEmail());
        mu.setFullName(domain.getFullName());
        mu.setRole(domain.getRole());
        mu.setToken(domain.getToken());
        return mu;
    }

    @Override
    public User create(MUser model) {
        boolean isClient;
        if (model.getRole() == null) {
            throw new BadRequestException("Missing parameter 'role'");
        } else if (model.getRole() == Role.SUPER) {
            throw new BadRequestException("Role 'super' can not be assign manually");
        } else {
            isClient = model.getRole() == Role.CLIENT;
        }
        if (!isClient && (model.getEmail() == null || model.getEmail().isEmpty())) {
            throw new BadRequestException("Missing parameter 'email'");
        }
        if (model.getFullName() == null || model.getFullName().isEmpty()) {
            throw new BadRequestException("Missing parameter 'fullName'");
        }
        if (!isClient && (model.getPassword() == null || model.getPassword().isEmpty())) {
            throw new BadRequestException("Missing parameter 'password'");
        }
        User u = null;
        String pass = null;
        if (!isClient) {
            try {
                u = getByEmail(model.getEmail());
                if (u != null && u.getDeleted() == null) {
                    throw new BadRequestException("There is already an user with email '" + model.getEmail() + "'");
                } else if (u != null && u.getDeleted() != null) {
                    u.setDeleted(null);
                    u.setUpdated(new Date());
                }
            } catch (NotFoundException ignored) {
            }
            pass = passwordEncoder.encode(model.getPassword());
        }
        if (u == null) {
            u = new User();
            u.setCreated(new Date());
            u.setEmail(model.getEmail());
        }
        u.setPassword(pass);
        u.setFullName(model.getFullName());
        u.setRole(model.getRole());
        return userRepository.save(u);
    }

    @Override
    public User update(User original, MUser edit) {
        if (original.getDeleted() != null) {
            throw new UnauthorizedException("This account has been suspended");
        }
        if (edit.getFullName() == null || edit.getFullName().isEmpty()) {
            throw new BadRequestException("Missing parameter 'fullName'");
        }
        original.setUpdated(new Date());
        original.setFullName(edit.getFullName());
        return userRepository.save(original);
    }

    @Override
    public User delete(User entity) {
        if (entity.getRole() == Role.SUPER) {
            throw new UnauthorizedException("Accounts with super role can not be deleted");
        }
        if (entity.getDeleted() != null) {
            throw new BadRequestException("This account has already been suspended");
        }
        entity.setDeleted(new Date());
        return userRepository.save(entity);
    }

    @Override
    public User get(Long id) {
        if (id == null) {
            throw new BadRequestException("Invalid id value");
        }
        return userRepository.findById(id).orElseThrow(() -> new NotFoundException("User not found. Id " + id));
    }

    @Override
    public Page<User> search(String term, Pageable pageable) {
        if (term == null || term.isEmpty()) {
            return userRepository.findByDeletedIsNull(pageable);
        } else {
            return userRepository.searchUsers(term, pageable);
        }
    }

}
