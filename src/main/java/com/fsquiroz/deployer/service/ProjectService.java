package com.fsquiroz.deployer.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fsquiroz.deployer.entity.db.Project;
import com.fsquiroz.deployer.entity.db.Role;
import com.fsquiroz.deployer.entity.db.SshKeyPair;
import com.fsquiroz.deployer.entity.db.User;
import com.fsquiroz.deployer.entity.json.MProject;
import com.fsquiroz.deployer.exception.*;
import com.fsquiroz.deployer.repository.ProjectRepository;
import com.fsquiroz.deployer.service.declaration.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@Profile("!test")
public class ProjectService implements IProjectService {

    private Map<Long, ProjectExecutor> executors;

    private ISshKeyPairService sshKeyPairService;

    private IBuildService buildService;

    private IGitService gitService;

    private IMavenService mavenService;

    private ISchedulerService schedulerService;

    private ProjectRepository projectRepository;

    private String projectPath;

    public ProjectService(
            ISshKeyPairService sshKeyPairService,
            IBuildService buildService,
            IGitService gitService,
            IMavenService mavenService,
            ISchedulerService schedulerService,
            ProjectRepository projectRepository,
            @Value("${com.fsquiroz.deployer.projects.path}") final String projectPath
    ) {
        this.executors = new HashMap<>();
        this.sshKeyPairService = sshKeyPairService;
        this.buildService = buildService;
        this.gitService = gitService;
        this.mavenService = mavenService;
        this.schedulerService = schedulerService;
        this.projectRepository = projectRepository;
        this.projectPath = projectPath;
    }

    @Override
    public Project create(MProject model, SshKeyPair key) {
        validateMusts(model);
        if (model.getAlias() == null || model.getAlias().isEmpty()) {
            throw new BadRequestException("Missing parameter 'alias'");
        }
        if (model.getUrl() == null || model.getUrl().isEmpty()) {
            throw new BadRequestException("Missing parameter 'url'");
        }
        if (key != null && key.getDeleted() != null) {
            throw new BadRequestException("Can not update project with a deleted key");
        }
        Project p = new Project();
        p.setCreated(new Date());
        p.setAlias(model.getAlias());
        p.setName(model.getName());
        p.setUrl(model.getUrl());
        p.setBranch(model.getBranch());
        p.setGoals(model.getGoals());
        p.setTargetPath(model.getTargetPath());
        p.setPackagedPath(model.getPackagedPath());
        p.setKey(key);
        p = projectRepository.save(p);
        try {
            gitService.clone(p);
            mavenService.validatePom(p);
        } catch (Exception e) {
            projectRepository.delete(p);
            throw new ServerException(e);
        }
        return p;
    }

    @Override
    public Project update(Project original, MProject edit, SshKeyPair key) {
        if (executors.containsKey(original.getId())) {
            throw new BadRequestException("There is already a process running for this project. The process needs to finish to perform ay action on it");
        }
        if (key != null && key.getDeleted() != null) {
            throw new BadRequestException("Can not update project with a deleted key");
        }
        validateMusts(edit);
        original.setUpdated(new Date());
        original.setDeleted(null);
        original.setName(edit.getName());
        original.setBranch(edit.getBranch());
        original.setKey(key);
        original.setGoals(edit.getGoals());
        original.setPackagedPath(edit.getPackagedPath());
        original.setTargetPath(edit.getTargetPath());
        gitService.update(original);
        mavenService.validatePom(original);
        return projectRepository.save(original);
    }

    private void validateMusts(MProject mp) {
        if (mp.getName() == null || mp.getName().isEmpty()) {
            throw new BadRequestException("Missing parameter 'name'");
        }
        if (mp.getBranch() == null || mp.getBranch().isEmpty()) {
            throw new BadRequestException("Missing parameter 'branch'");
        }
        if (mp.getGoals() == null || mp.getGoals().isEmpty()) {
            throw new BadRequestException("Missing parameter 'goals'");
        }
        if (mp.getPackagedPath() == null || mp.getPackagedPath().isEmpty()) {
            throw new BadRequestException("Missing parameter 'packagedPath'");
        }
        if (mp.getTargetPath() == null || mp.getTargetPath().isEmpty()) {
            throw new BadRequestException("Missing parameter 'targetPath'");
        }
    }

    @Override
    public Project delete(Project entity) {
        if (executors.containsKey(entity.getId())) {
            throw new BadRequestException("There is already a process running for this project. The process needs to finish to perform ay action on it");
        }
        if (entity.getDeleted() != null) {
            throw new BadRequestException("This project has already been deleted");
        }
        entity.setDeleted(new Date());
        entity.setKey(null);
        return projectRepository.save(entity);
    }

    @Override
    public Project get(Long id) {
        return projectRepository.findById(id).orElseThrow(() -> new NotFoundException("Project not found. Id " + id));
    }

    @Override
    public Page<Project> search(String term, Pageable pageable) {
        if (term == null || term.isEmpty()) {
            return projectRepository.findByDeletedIsNull(pageable);
        } else {
            return projectRepository.searchProjects(term, pageable);
        }
    }

    @Override
    public void triggerUpdate(User user, Project p, JsonNode json) {
        this.triggerUpdate(user, p, json, true);
    }

    protected void triggerUpdate(User user, Project p, JsonNode json, boolean trigger) {
        if (p.getDeleted() != null) {
            throw new BadRequestException("Project deleted.");
        }
        if (user.getRole() == Role.CLIENT && user.getProjectId() != null && !user.getProjectId().equals(p.getId())) {
            throw new ForbidenException("This account does not have privileges to trigger an update on this project");
        }
        if (executors.containsKey(p.getId())) {
            throw new BadRequestException("There is already a process running for this project. The process needs to finish to perform ay action on it");
        }
        if (json != null) {
            if (json.has("object_kind")) {
                String kind = json.get("object_kind").asText();
                if (!"push".equals(kind)) {
                    throw new BadRequestException("Event '" + kind + "' not supported");
                }
            }
            if (json.has("ref")) {
                String ref = json.get("ref").asText();
                if (!ref.endsWith(p.getBranch())) {
                    throw new BadRequestException("Branch '" + ref + "' not supported");
                }
            }
        }
        if (trigger) {
            schedulerService.addExecutor(new ProjectExecutor(p, executors, gitService, mavenService, buildService, projectPath));
        }
    }

    @Override
    public MProject build(Project domain) {
        MProject mp = new MProject();
        mp.setId(domain.getId());
        mp.setCreated(domain.getCreated());
        mp.setUpdated(domain.getUpdated());
        mp.setDeleted(domain.getDeleted());
        mp.setName(domain.getName());
        mp.setAlias(domain.getAlias());
        mp.setUrl(domain.getUrl());
        mp.setBranch(domain.getBranch());
        mp.setTargetPath(domain.getTargetPath());
        mp.setGoals(domain.getGoals());
        mp.setPackagedPath(domain.getPackagedPath());
        if (domain.getKey() != null) {
            mp.setKey(sshKeyPairService.build(domain.getKey(), false));
        }
        return mp;
    }

}
