package com.fsquiroz.deployer.service;

import com.fsquiroz.deployer.entity.db.Build;
import com.fsquiroz.deployer.entity.db.Project;
import com.fsquiroz.deployer.entity.json.MBuild;
import com.fsquiroz.deployer.exception.NotFoundException;
import com.fsquiroz.deployer.repository.BuildRepository;
import com.fsquiroz.deployer.service.declaration.IBuildService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class BuildService implements IBuildService {

    private BuildRepository buildRepository;

    public BuildService(BuildRepository buildRepository) {
        this.buildRepository = buildRepository;
    }

    @Override
    public Build create(Project project) {
        Build b = new Build();
        b.setCreated(new Date());
        b.setProject(project);
        b.setStart(new Date());
        return buildRepository.save(b);
    }

    @Override
    public Build update(Build original, MBuild edit) {
        original.setUpdated(new Date());
        original.setMessage(edit.getMessage());
        original.setEnd(edit.getEnd());
        original.setStatus(edit.getStatus());
        return buildRepository.save(original);
    }

    @Override
    public Build get(Long id) {
        return buildRepository.findById(id).orElseThrow(() -> new NotFoundException("Build not found" + id));
    }

    @Override
    public Build getLast(Project project) {
        Pageable pg = PageRequest.of(0, 1, Sort.Direction.DESC, "updated");
        Page<Build> res = buildRepository.findByDeletedIsNull(pg);
        if (res.getContent().isEmpty()) {
            return null;
        } else {
            return res.getContent().get(0);
        }
    }

    @Override
    public Page<Build> search(Project project, Pageable pageable) {
        if (project == null) {
            return buildRepository.findByDeletedIsNull(pageable);
        } else {
            return buildRepository.findByDeletedIsNullAndProject(project, pageable);
        }
    }

    @Override
    public MBuild build(Build domain) {
        MBuild mb = new MBuild();
        mb.setId(domain.getId());
        mb.setCreated(domain.getCreated());
        mb.setUpdated(domain.getUpdated());
        mb.setDeleted(domain.getDeleted());
        mb.setStart(domain.getStart());
        mb.setEnd(domain.getEnd());
        mb.setStatus(domain.getStatus());
        mb.setMessage(domain.getMessage());
        if (domain.getProject() != null) {
            mb.setProjectId(domain.getProject().getId());
            mb.setProjectName(domain.getProject().getName());
        }
        return mb;
    }

}
