package com.fsquiroz.deployer.service;

import com.fsquiroz.deployer.entity.db.Settings;
import com.fsquiroz.deployer.entity.json.MSettings;
import com.fsquiroz.deployer.exception.BadRequestException;
import com.fsquiroz.deployer.exception.NotFoundException;
import com.fsquiroz.deployer.repository.SettingsRepository;
import com.fsquiroz.deployer.service.declaration.ISettingsService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class SettingsService implements ISettingsService {

    private SettingsRepository settingsRepository;

    public SettingsService(SettingsRepository settingsRepository) {
        this.settingsRepository = settingsRepository;
    }

    @Override
    public Settings getLast() {
        Pageable pg = PageRequest.of(0, 1, Sort.Direction.DESC, "timestamp");
        Page<Settings> res = settingsRepository.findAll(pg);
        if (res.getContent().isEmpty()) {
            throw new NotFoundException("There are no settings");
        } else {
            return res.getContent().get(0);
        }
    }

    @Override
    public Settings update(MSettings ms) {
        if (ms.getJavaHome() == null || ms.getJavaHome().isEmpty()) {
            throw new BadRequestException("Missing parameter 'javaHome'");
        }
        if (ms.getMavenHome() == null || ms.getMavenHome().isEmpty()) {
            throw new BadRequestException("Missing parameter 'mavenHome'");
        }
        Settings s = new Settings();
        s.setCreated(new Date());
        s.setTimestamp(s.getCreated().getTime());
        s.setJavaHome(ms.getJavaHome());
        s.setMavenHome(ms.getMavenHome());
        return settingsRepository.save(s);
    }

    @Override
    public MSettings build(Settings domain) {
        MSettings ms = new MSettings();
        ms.setLastUpdate(domain.getCreated());
        ms.setJavaHome(domain.getJavaHome());
        ms.setMavenHome(domain.getMavenHome());
        return ms;
    }

}
