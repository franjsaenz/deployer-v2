package com.fsquiroz.deployer.controller;

import com.fsquiroz.deployer.entity.db.Settings;
import com.fsquiroz.deployer.entity.json.MSettings;
import com.fsquiroz.deployer.service.declaration.ISettingsService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/settings")
@RequiredArgsConstructor
public class SettingsController {

    private final ISettingsService settingsService;

    @GetMapping
    @PreAuthorize("hasRole('SUPER')")
    public ResponseEntity<MSettings> get() {
        Settings s = settingsService.getLast();
        return ResponseEntity.ok(settingsService.build(s));
    }

    @PostMapping
    @PreAuthorize("hasRole('SUPER')")
    public ResponseEntity<MSettings> update(@RequestBody MSettings settings) {
        Settings s = settingsService.update(settings);
        return new ResponseEntity<>(settingsService.build(s), HttpStatus.CREATED);
    }

}
