package com.fsquiroz.deployer.controller;

import com.fsquiroz.deployer.entity.db.Project;
import com.fsquiroz.deployer.entity.db.User;
import com.fsquiroz.deployer.entity.json.MResetPassword;
import com.fsquiroz.deployer.entity.json.MResponse;
import com.fsquiroz.deployer.entity.json.MUser;
import com.fsquiroz.deployer.service.declaration.IProjectService;
import com.fsquiroz.deployer.service.declaration.IUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserController {

    private final IUserService userService;

    private final IProjectService projectService;

    @GetMapping
    public ResponseEntity<Page<MUser>> search(@RequestParam(required = false) String term, Pageable pageable) {
        Page<User> users = userService.search(term, pageable);
        return ResponseEntity.ok(userService.build(users));
    }

    @PostMapping
    public ResponseEntity<MUser> create(@RequestBody MUser user) {
        User u = userService.create(user);
        return new ResponseEntity<>(userService.build(u), HttpStatus.CREATED);
    }

    @GetMapping("/{userId}")
    public ResponseEntity<MUser> get(@PathVariable Long userId) {
        User u = userService.get(userId);
        return ResponseEntity.ok(userService.build(u));
    }

    @PutMapping("/{userId}")
    public ResponseEntity<MUser> update(@PathVariable Long userId, @RequestBody MUser user) {
        User u = userService.get(userId);
        u = userService.update(u, user);
        return ResponseEntity.ok(userService.build(u));
    }

    @DeleteMapping("/{userId}")
    public ResponseEntity<MUser> delete(@PathVariable Long userId) {
        User u = userService.get(userId);
        u = userService.delete(u);
        return ResponseEntity.ok(userService.build(u));
    }

    @PutMapping("/{userId}/password")
    public ResponseEntity<MResponse> updatePassword(@PathVariable Long userId, @RequestBody MResetPassword password) {
        User u = userService.get(userId);
        userService.changePassword(u, password);
        return ResponseEntity.ok(MResponse.of("Password updated"));
    }

    @GetMapping("/{userId}/token")
    public ResponseEntity<MUser> generateToken(
            @PathVariable Long userId,
            @RequestParam(required = false) Long projectId,
            @RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ") Date expiration,
            @RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ") Date notBefore
    ) {
        User u = userService.get(userId);
        Project p = null;
        if (projectId != null) {
            p = projectService.get(projectId);
        }
        u = userService.generateToken(u, p, expiration, notBefore);
        return ResponseEntity.ok(userService.build(u));
    }

}
