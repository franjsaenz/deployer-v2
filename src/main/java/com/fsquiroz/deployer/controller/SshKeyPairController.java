package com.fsquiroz.deployer.controller;

import com.fsquiroz.deployer.entity.db.SshKeyPair;
import com.fsquiroz.deployer.entity.json.MSshKeyPair;
import com.fsquiroz.deployer.service.declaration.ISshKeyPairService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/keys")
@RequiredArgsConstructor
public class SshKeyPairController {

    private final ISshKeyPairService sshKeyPairService;

    @GetMapping
    public ResponseEntity<Page<MSshKeyPair>> search(@RequestParam(required = false) String term, Pageable pageable) {
        Page<SshKeyPair> keys = sshKeyPairService.search(term, pageable);
        return ResponseEntity.ok(sshKeyPairService.build(keys));
    }

    @PostMapping
    public ResponseEntity<MSshKeyPair> create(@RequestBody MSshKeyPair key) {
        SshKeyPair skp = sshKeyPairService.create(key);
        return new ResponseEntity<>(sshKeyPairService.build(skp, true), HttpStatus.CREATED);
    }

    @GetMapping("/{sshKeyId}")
    @PreAuthorize("hasAnyRole('SUPER', 'ADMIN')")
    public ResponseEntity<MSshKeyPair> get(@PathVariable Long sshKeyId) {
        SshKeyPair key = sshKeyPairService.get(sshKeyId);
        return ResponseEntity.ok(sshKeyPairService.build(key, true));
    }

    @DeleteMapping("/{sshKeyId}")
    public ResponseEntity<MSshKeyPair> delete(@PathVariable Long sshKeyId) {
        SshKeyPair skp = sshKeyPairService.get(sshKeyId);
        skp = sshKeyPairService.delete(skp);
        return ResponseEntity.ok(sshKeyPairService.build(skp, false));
    }

}
