package com.fsquiroz.deployer.controller;

import com.fsquiroz.deployer.entity.db.User;
import com.fsquiroz.deployer.entity.json.MLogin;
import com.fsquiroz.deployer.entity.json.MResponse;
import com.fsquiroz.deployer.entity.json.MSetup;
import com.fsquiroz.deployer.entity.json.MUser;
import com.fsquiroz.deployer.service.declaration.IUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController
@RequestMapping
@RequiredArgsConstructor
public class PublicController {

    private final IUserService userService;

    @PostMapping("/login")
    public ResponseEntity<MUser> login(
            @RequestBody MLogin login,
            @RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ") Date expiration,
            @RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ") Date notBefore
    ) {
        User u = userService.login(login, expiration, notBefore);
        return ResponseEntity.ok(userService.build(u));
    }

    @PostMapping("/setup")
    public ResponseEntity<MResponse> setup(@RequestBody MSetup setup) {
        userService.setup(setup);
        return ResponseEntity.ok(MResponse.of("Setup ended successfully. Password was printed in console, it is advisable to change it as soon as possible"));
    }

}
