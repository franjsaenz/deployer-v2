package com.fsquiroz.deployer.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fsquiroz.deployer.entity.db.Build;
import com.fsquiroz.deployer.entity.db.Project;
import com.fsquiroz.deployer.entity.db.SshKeyPair;
import com.fsquiroz.deployer.entity.db.User;
import com.fsquiroz.deployer.entity.json.MBuild;
import com.fsquiroz.deployer.entity.json.MProject;
import com.fsquiroz.deployer.entity.json.MResponse;
import com.fsquiroz.deployer.exception.NotFoundException;
import com.fsquiroz.deployer.security.SecurityCheck;
import com.fsquiroz.deployer.service.declaration.IBuildService;
import com.fsquiroz.deployer.service.declaration.IProjectService;
import com.fsquiroz.deployer.service.declaration.ISshKeyPairService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/projects")
@RequiredArgsConstructor
public class ProjectController {

    private final IProjectService projectService;

    private final ISshKeyPairService sshKeyPairService;

    private final IBuildService buildService;

    private final SecurityCheck securityCheck;

    @GetMapping
    @PreAuthorize("hasAnyRole('SUPER', 'ADMIN')")
    public ResponseEntity<Page<MProject>> search(@RequestParam(required = false) String term, Pageable pageable) {
        Page<Project> result = projectService.search(term, pageable);
        return ResponseEntity.ok(projectService.build(result));
    }

    @PostMapping
    public ResponseEntity<MProject> create(@RequestBody MProject mp, @RequestParam(required = false) Long sshKeyId) {
        SshKeyPair skp = null;
        if (sshKeyId != null) {
            skp = sshKeyPairService.get(sshKeyId);
        }
        Project p = projectService.create(mp, skp);
        return new ResponseEntity<>(projectService.build(p), HttpStatus.CREATED);
    }

    @GetMapping("/{projectId}")
    @PreAuthorize("hasAnyRole('SUPER', 'ADMIN')")
    public ResponseEntity<MProject> get(@PathVariable Long projectId) {
        Project p = projectService.get(projectId);
        return ResponseEntity.ok(projectService.build(p));
    }

    @PutMapping("/{projectId}")
    public ResponseEntity<MProject> update(@PathVariable Long projectId, @RequestBody MProject mp, @RequestParam(required = false) Long sshKeyId) {
        SshKeyPair skp = null;
        if (sshKeyId != null) {
            skp = sshKeyPairService.get(sshKeyId);
        }
        Project p = projectService.get(projectId);
        p = projectService.update(p, mp, skp);
        return ResponseEntity.ok(projectService.build(p));
    }

    @DeleteMapping("/{projectId}")
    public ResponseEntity<MProject> delete(@PathVariable Long projectId) {
        Project p = projectService.get(projectId);
        p = projectService.delete(p);
        return ResponseEntity.ok(projectService.build(p));
    }

    @GetMapping("/{projectId}/builds")
    @PreAuthorize("hasAnyRole('SUPER', 'ADMIN')")
    public ResponseEntity<Page<MBuild>> getBuilds(@PathVariable Long projectId, Pageable pageable) {
        Project p = projectService.get(projectId);
        Page<Build> builds = buildService.search(p, pageable);
        return ResponseEntity.ok(buildService.build(builds));
    }

    @PostMapping("/{projectId}/builds")
    public ResponseEntity<MResponse> build(@PathVariable Long projectId, @RequestBody(required = false) JsonNode json) {
        User u = securityCheck.getAuthentication();
        Project p = projectService.get(projectId);
        projectService.triggerUpdate(u, p, json);
        return ResponseEntity.ok(MResponse.of("Build triggered"));
    }

    @GetMapping("/{projectId}/builds/last")
    @PreAuthorize("hasAnyRole('SUPER', 'ADMIN')")
    public ResponseEntity<MBuild> getLastBuild(@PathVariable Long projectId) {
        Project p = projectService.get(projectId);
        Build b = buildService.getLast(p);
        if (b != null) {
            return ResponseEntity.ok(buildService.build(b));
        } else {
            throw new NotFoundException("There are no builds available for this project");
        }
    }

}
