package com.fsquiroz.deployer.controller;

import com.fsquiroz.deployer.entity.db.Build;
import com.fsquiroz.deployer.entity.json.MBuild;
import com.fsquiroz.deployer.service.declaration.IBuildService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/builds")
@RequiredArgsConstructor
public class BuildController {

    private final IBuildService buildService;

    @GetMapping
    @PreAuthorize("hasAnyRole('SUPER', 'ADMIN')")
    public ResponseEntity<Page<MBuild>> search(Pageable pageable) {
        Page<Build> builds = buildService.search(null, pageable);
        return ResponseEntity.ok(buildService.build(builds));
    }

    @GetMapping("/{buildId}")
    @PreAuthorize("hasAnyRole('SUPER', 'ADMIN')")
    public ResponseEntity<MBuild> get(@PathVariable Long buildId) {
        Build b = buildService.get(buildId);
        return ResponseEntity.ok(buildService.build(b));
    }

}
