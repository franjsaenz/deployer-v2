package com.fsquiroz.deployer.controller;

import com.fsquiroz.deployer.entity.json.MException;
import com.fsquiroz.deployer.exception.AppException;
import com.fsquiroz.deployer.security.SecurityCheck;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.catalina.connector.ClientAbortException;
import org.springframework.core.annotation.AnnotatedElementUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;


import java.util.Date;

@Slf4j
@RequiredArgsConstructor
@RestControllerAdvice(annotations = RestController.class)
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {

    private final SecurityCheck securityCheck;

    @ExceptionHandler(AppException.class)
    public ResponseEntity<MException> appException(HttpServletRequest req, AppException ae) {
        logUrl(req);
        log.debug(ae.getMessage(), ae);
        return parseException(req, ae);
    }

    @ExceptionHandler(ClientAbortException.class)
    public void clientAbortException(HttpServletRequest req, ClientAbortException e) {
        logUrl(req);
        log.debug(e.getMessage(), e);
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<MException> accessDeniedException(HttpServletRequest req, AccessDeniedException e) {
        HttpStatus status = HttpStatus.FORBIDDEN;
        try {
            securityCheck.getAuthentication();
        } catch (AppException ae) {
            status = HttpStatus.UNAUTHORIZED;
        }
        logUrl(req);
        return build(req, status, e.getMessage());
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<MException> genericException(HttpServletRequest req, Exception e) {
        if (req.getQueryString() != null && !req.getQueryString().isEmpty()) {
            log.error("[{}] {}?{}", req.getMethod(), req.getRequestURL(), req.getQueryString());
        } else {
            log.error("[{}] {}", req.getMethod(), req.getRequestURL());
        }
        log.error(e.getMessage(), e);
        return parseException(req, e);
    }


    private void logUrl(HttpServletRequest req) {
        if (req.getQueryString() != null && !req.getQueryString().isEmpty()) {
            log.debug("[{}] {}?{}", req.getMethod(), req.getRequestURL(), req.getQueryString());
        } else {
            log.debug("[{}] {}", req.getMethod(), req.getRequestURL());
        }
    }

    private ResponseEntity<MException> parseException(HttpServletRequest req, Exception e) {
        ResponseStatus rs = AnnotatedElementUtils.findMergedAnnotation(e.getClass(), ResponseStatus.class);
        HttpStatus status;
        if (rs != null) {
            status = rs.value();
        } else {
            status = HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return build(req, status, status == HttpStatus.INTERNAL_SERVER_ERROR ? "There has been an unexpected error" : e.getMessage());
    }


    private ResponseEntity<MException> build(HttpServletRequest req, HttpStatus status, String message) {
        MException me = new MException(
                new Date(),
                status.value(),
                status.getReasonPhrase(),
                message,
                req.getRequestURI()
        );
        return new ResponseEntity<>(
                me,
                status
        );
    }

}
