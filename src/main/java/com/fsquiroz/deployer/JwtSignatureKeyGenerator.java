package com.fsquiroz.deployer;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.MacAlgorithm;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.security.Key;
import java.util.regex.Pattern;

@Slf4j
public class JwtSignatureKeyGenerator {

    private static final String USER_HOME = System.getProperty("user.home");
    private static final String SEPARATOR = File.separator;
    private static final String CONTEXT_PATH = USER_HOME + SEPARATOR + ".deployer";

    private static final Pattern JWT_SECRET_REGEX = Pattern.compile("^security\\.jwt\\.secret=.*$", Pattern.MULTILINE);
    private static final Pattern JWT_ALG_REGEX = Pattern.compile("^security\\.jwt\\.alg=.*$", Pattern.MULTILINE);

    private final File applicationContextPath;

    private final File applicationPropertyPath;

    private final File applicationPropertyOldPath;

    private String applicationProperties;

    private JwtSignatureKeyGenerator() {
        applicationContextPath = new File(CONTEXT_PATH);
        applicationPropertyPath = new File(CONTEXT_PATH + SEPARATOR + "application.properties");
        applicationPropertyOldPath = new File(CONTEXT_PATH + SEPARATOR + "application.properties.old");
        applicationProperties = loadApplicationProperties();
    }

    @SneakyThrows
    private String loadApplicationProperties() {
        if (applicationPropertyPath.exists()) {
            byte[] bytes = Files.readAllBytes(applicationPropertyPath.toPath());
            return new String(bytes, StandardCharsets.UTF_8);
        } else {
            return "";
        }
    }

    public void generateIfNecessary() {
        createContextFolderIfMissing();
        if (applicationPropertiesIsUpToDate()) {
            log.info("Application property is up to date");
        } else {
            log.info("Application property not is up to date, updating it");
            commentOldProperties();
            addProperties();
            backUp();
            saveNewFile();
        }
    }

    private void createContextFolderIfMissing() {
        boolean exists = contextFolderExists();
        if (!exists) {
            log.info("Creating application context folder under {}", applicationContextPath.getAbsolutePath());
            boolean result = applicationContextPath.mkdirs();
            if (!result) {
                log.error("Failed to create context folder");
                throw new RuntimeException("Failed to create context folder");
            }
            log.info("Application context folder created");
        }
    }

    private boolean contextFolderExists() {
        log.info("Checking if application context folder exists under {}", applicationContextPath.getAbsolutePath());
        boolean exists = applicationContextPath.exists();
        if (exists && !applicationContextPath.isDirectory()) {
            log.error("Application context folder exists but is not a directory");
            throw new RuntimeException("Application context folder exists but is not a directory");
        }
        return exists;
    }

    private boolean applicationPropertiesIsUpToDate() {
        return JWT_SECRET_REGEX.matcher(applicationProperties).find() && JWT_ALG_REGEX.matcher(applicationProperties).find();
    }

    private void commentOldProperties() {
        applicationProperties = JWT_SECRET_REGEX.matcher(applicationProperties).replaceAll("# $0");
        applicationProperties = JWT_ALG_REGEX.matcher(applicationProperties).replaceAll("# $0");
    }

    private void addProperties() {
        MacAlgorithm alg = Jwts.SIG.HS512;
        Key key = alg.key().build();
        String secret = Base64.encodeBase64String(key.getEncoded());
        applicationProperties = applicationProperties + "\n" +
                "# JWT properties\n" +
                "security.jwt.secret=" + secret + "\n" +
                "security.jwt.alg=" + key.getAlgorithm() + "\n";
    }

    @SneakyThrows
    private void backUp() {
        if (applicationPropertyPath.exists()) {
            log.info("Backing up application properties to application.properties.old");
            Files.move(applicationPropertyPath.toPath(), applicationPropertyOldPath.toPath(), StandardCopyOption.REPLACE_EXISTING);
        }
    }

    @SneakyThrows
    private void saveNewFile() {
        log.info("Saving new application properties");
        Files.write(applicationPropertyPath.toPath(), applicationProperties.getBytes(StandardCharsets.UTF_8), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
    }

    public static JwtSignatureKeyGenerator build() {
        return new JwtSignatureKeyGenerator();
    }
}
