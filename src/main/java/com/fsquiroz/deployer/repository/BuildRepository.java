package com.fsquiroz.deployer.repository;

import com.fsquiroz.deployer.entity.db.Build;
import com.fsquiroz.deployer.entity.db.Project;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BuildRepository extends JpaRepository<Build, Long> {

    Page<Build> findByDeletedIsNullAndProject(Project project, Pageable pageable);

    Page<Build> findByDeletedIsNull(Pageable pageable);

}
