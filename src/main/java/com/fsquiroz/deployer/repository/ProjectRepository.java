package com.fsquiroz.deployer.repository;

import com.fsquiroz.deployer.entity.db.Project;
import com.fsquiroz.deployer.entity.db.SshKeyPair;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Long> {

    Project findFirstByKey(SshKeyPair key);

    @Query("select p from Project p where p.deleted is null and (p.alias like %?1% or p.name like %?1%)")
    Page<Project> searchProjects(String term, Pageable pageable);

    Page<Project> findByDeletedIsNull(Pageable pageable);

    Page<Project> findByKeyAndDeletedIsNull(SshKeyPair key, Pageable pageable);

}
