package com.fsquiroz.deployer.repository;

import com.fsquiroz.deployer.entity.db.SshKeyPair;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface SshKeyPairRepository extends JpaRepository<SshKeyPair, Long> {

    @Query("select k from SshKeyPair k where k.alias like %?1% and k.deleted is null ")
    Page<SshKeyPair> searchKeys(String term, Pageable pageable);

    Page<SshKeyPair> findByDeletedIsNull(Pageable pageable);

}
