package com.fsquiroz.deployer.repository;

import com.fsquiroz.deployer.entity.db.Settings;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SettingsRepository extends JpaRepository<Settings, Long> {
}
