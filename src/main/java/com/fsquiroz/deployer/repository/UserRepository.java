package com.fsquiroz.deployer.repository;

import com.fsquiroz.deployer.entity.db.Role;
import com.fsquiroz.deployer.entity.db.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    User findFirstByDeletedIsNullAndRoleOrderByCreatedDesc(Role role);

    Optional<User> findByEmail(String email);

    @Query("select u from User u where u.deleted is null  and (u.email like %?1% or u.fullName like %?1%)")
    Page<User> searchUsers(String term, Pageable pageable);

    Page<User> findByDeletedIsNull(Pageable pageable);

}
