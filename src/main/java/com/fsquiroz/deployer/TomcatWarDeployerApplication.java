package com.fsquiroz.deployer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.data.web.config.EnableSpringDataWebSupport;

import static org.springframework.data.web.config.EnableSpringDataWebSupport.PageSerializationMode.VIA_DTO;

@SpringBootApplication
@EnableSpringDataWebSupport(pageSerializationMode = VIA_DTO)
public class TomcatWarDeployerApplication extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        JwtSignatureKeyGenerator.build()
                .generateIfNecessary();
        return application.sources(TomcatWarDeployerApplication.class);
    }

    public static void main(String[] args) {
        JwtSignatureKeyGenerator.build()
                .generateIfNecessary();
        SpringApplication.run(TomcatWarDeployerApplication.class, args);
    }

}
