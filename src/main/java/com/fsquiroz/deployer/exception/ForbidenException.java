package com.fsquiroz.deployer.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class ForbidenException extends AppException {

    public ForbidenException(String message) {
        super(message);
    }

    public ForbidenException(String message, Throwable cause) {
        super(message, cause);
    }

    public ForbidenException(Throwable cause) {
        super(cause);
    }

}
